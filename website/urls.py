from django.contrib.staticfiles.urls import staticfiles_urlpatterns
from django.conf.urls import patterns, include, url
from django.conf import settings
from django.contrib import admin
from django.views.generic import TemplateView

admin.autodiscover()

urlpatterns = patterns('',
    url(r'^party/$', 'website.views.party', name='party'),

    #url(r'^party/(?P<pm_id>.*)/$', 'website.views.party', name='party'),
    #url(r'^politician/(?P<pm_id>.*)/$', 'website.views.politician', name='politician'),
    url(r'^proceeding/(?P<pm_id>.*)/$', 'website.views.proceeding', name='proceeding'),
    url(r'^compare/(?P<pm_id1>[^/]+)/(?P<pm_id2>[^/]+)/$', 'website.views.compare', name='compare'),
    url(r'^government/$', 'website.views.government', name='government'),

    url(r'^resolver/(?P<pm_id>.*)/$', 'website.views.ajax_resolver', name='resolver'),

    url(r'^json/politician/(?P<pm_id>[^/]+)?/?$', 'website.views.politician_json', name='json-politician'),
    url(r'^json/party/(?P<pm_id>[^/]+)?/?$', 'website.views.party_json', name='json-party'),

    url(r'^robots.txt$', TemplateView.as_view(template_name='robots.txt')),
    url(r'^humans.txt$', TemplateView.as_view(template_name='humans.txt')),

    # Uncomment the next line to enable the admin:
    url(r'^admin/', include(admin.site.urls)),

    url(r'^page/', include('cms.urls')),

)

if settings.DEBUG:
    urlpatterns += staticfiles_urlpatterns()
    urlpatterns += patterns('',
        url(r'^media/(?P<path>.*)$', 'django.views.static.serve', {
            'document_root': settings.MEDIA_ROOT,
        }),
   )

urlpatterns += patterns('',
    url(r'^eerstekamer/$', 'website.views.eerstekamer'),
    url(r'^tweedekamer/$', 'website.views.tweedekamer'),
    url(r'^regering/$', 'website.views.regering'),
    url(r'^.*', 'website.views.homepage', name='homepage'),
)