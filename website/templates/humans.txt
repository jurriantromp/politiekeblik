# humanstxt.org/
# The humans responsible & technology colophon

# TEAM

    Name: Jurrian Tromp
    Website: jurriantromp.nl
    Twitter: @jurriantromp

# THANKS

    Name: Maarten Marx
    For: Open data
    Site: Polititicalmashup.nl


# TECHNOLOGY COLOPHON

    HTML5, CSS3
    EmberJS, D3, jQuery, Modernizr
