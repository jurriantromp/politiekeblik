# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding unique constraint on 'Party', fields ['name']
        db.create_unique(u'website_party', ['name'])

        # Adding unique constraint on 'Party', fields ['pm_id']
        db.create_unique(u'website_party', ['pm_id'])

        # Adding unique constraint on 'Dossier', fields ['pm_id']
        db.create_unique(u'website_dossier', ['pm_id'])

        # Adding unique constraint on 'Politician', fields ['name']
        db.create_unique(u'website_politician', ['name'])

        # Adding unique constraint on 'Politician', fields ['pm_id']
        db.create_unique(u'website_politician', ['pm_id'])

        # Adding unique constraint on 'Proceeding', fields ['pm_id']
        db.create_unique(u'website_proceeding', ['pm_id'])


    def backwards(self, orm):
        # Removing unique constraint on 'Proceeding', fields ['pm_id']
        db.delete_unique(u'website_proceeding', ['pm_id'])

        # Removing unique constraint on 'Politician', fields ['pm_id']
        db.delete_unique(u'website_politician', ['pm_id'])

        # Removing unique constraint on 'Politician', fields ['name']
        db.delete_unique(u'website_politician', ['name'])

        # Removing unique constraint on 'Dossier', fields ['pm_id']
        db.delete_unique(u'website_dossier', ['pm_id'])

        # Removing unique constraint on 'Party', fields ['pm_id']
        db.delete_unique(u'website_party', ['pm_id'])

        # Removing unique constraint on 'Party', fields ['name']
        db.delete_unique(u'website_party', ['name'])


    models = {
        u'website.dossier': {
            'Meta': {'object_name': 'Dossier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']", 'null': 'True', 'blank': 'True'}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'object_name': 'Politician'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'dossier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            'file_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'file_subnumber': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']", 'null': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']