# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Removing unique constraint on 'Party', fields ['name']
        db.delete_unique(u'website_party', ['name'])


        # Changing field 'Party.pm_id'
        db.alter_column(u'website_party', 'pm_id', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=30))

        # Changing field 'Dossier.pm_id'
        db.alter_column(u'website_dossier', 'pm_id', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=30))

        # Changing field 'Politician.pm_id'
        db.alter_column(u'website_politician', 'pm_id', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=30))

        # Changing field 'Proceeding.pm_id'
        db.alter_column(u'website_proceeding', 'pm_id', self.gf('django.db.models.fields.CharField')(default='', unique=True, max_length=30))

    def backwards(self, orm):

        # Changing field 'Party.pm_id'
        db.alter_column(u'website_party', 'pm_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30, null=True))
        # Adding unique constraint on 'Party', fields ['name']
        db.create_unique(u'website_party', ['name'])


        # Changing field 'Dossier.pm_id'
        db.alter_column(u'website_dossier', 'pm_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30, null=True))

        # Changing field 'Politician.pm_id'
        db.alter_column(u'website_politician', 'pm_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30, null=True))

        # Changing field 'Proceeding.pm_id'
        db.alter_column(u'website_proceeding', 'pm_id', self.gf('django.db.models.fields.CharField')(unique=True, max_length=30, null=True))

    models = {
        u'website.dossier': {
            'Meta': {'object_name': 'Dossier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'db_index': 'True'}),
            'pm_source': ('django.db.models.fields.URLField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'db_index': 'True'}),
            'pm_source': ('django.db.models.fields.URLField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'unique_together': "(['vote', 'party'],)", 'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']", 'null': 'True', 'blank': 'True'}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'Politician'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '30', 'null': 'True', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'photo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'db_index': 'True'}),
            'pm_source': ('django.db.models.fields.URLField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'unique_together': "(['file_number', 'file_subnumber'],)", 'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'dossier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            'file_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'file_subnumber': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '30', 'db_index': 'True'}),
            'pm_source': ('django.db.models.fields.URLField', [], {'max_length': '60', 'null': 'True', 'blank': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']", 'null': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']