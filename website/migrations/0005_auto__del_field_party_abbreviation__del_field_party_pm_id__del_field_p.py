# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Party.abbreviation'
        db.delete_column(u'website_party', 'abbreviation')

        # Deleting field 'Party.pm_id'
        db.delete_column(u'website_party', 'pm_id')

        # Deleting field 'Party.pm_prefix'
        db.delete_column(u'website_party', 'pm_prefix')

        # Adding field 'Party.pma_prefix'
        db.add_column(u'website_party', 'pma_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Party.pma_id'
        db.add_column(u'website_party', 'pma_id',
                      self.gf('django.db.models.fields.IntegerField')(db_index=True, unique=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Party.pma_ref'
        db.add_column(u'website_party', 'pma_ref',
                      self.gf('django.db.models.fields.CharField')(db_index=True, max_length=60, unique=True, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Dossier.pm_id'
        db.delete_column(u'website_dossier', 'pm_id')

        # Deleting field 'Dossier.pm_prefix'
        db.delete_column(u'website_dossier', 'pm_prefix')

        # Adding field 'Dossier.pma_prefix'
        db.add_column(u'website_dossier', 'pma_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Dossier.pma_id'
        db.add_column(u'website_dossier', 'pma_id',
                      self.gf('django.db.models.fields.IntegerField')(db_index=True, unique=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Dossier.pma_ref'
        db.add_column(u'website_dossier', 'pma_ref',
                      self.gf('django.db.models.fields.CharField')(db_index=True, max_length=60, unique=True, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Politician.pm_id'
        db.delete_column(u'website_politician', 'pm_id')

        # Deleting field 'Politician.pm_prefix'
        db.delete_column(u'website_politician', 'pm_prefix')

        # Adding field 'Politician.pma_prefix'
        db.add_column(u'website_politician', 'pma_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Politician.pma_id'
        db.add_column(u'website_politician', 'pma_id',
                      self.gf('django.db.models.fields.IntegerField')(db_index=True, unique=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Politician.pma_ref'
        db.add_column(u'website_politician', 'pma_ref',
                      self.gf('django.db.models.fields.CharField')(db_index=True, max_length=60, unique=True, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Proceeding.pm_id'
        db.delete_column(u'website_proceeding', 'pm_id')

        # Deleting field 'Proceeding.pm_prefix'
        db.delete_column(u'website_proceeding', 'pm_prefix')

        # Adding field 'Proceeding.pma_prefix'
        db.add_column(u'website_proceeding', 'pma_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Proceeding.pma_id'
        db.add_column(u'website_proceeding', 'pma_id',
                      self.gf('django.db.models.fields.IntegerField')(db_index=True, unique=True, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Proceeding.pma_ref'
        db.add_column(u'website_proceeding', 'pma_ref',
                      self.gf('django.db.models.fields.CharField')(db_index=True, max_length=60, unique=True, null=True, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Party.abbreviation'
        db.add_column(u'website_party', 'abbreviation',
                      self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True),
                      keep_default=False)

        # Adding field 'Party.pm_id'
        db.add_column(u'website_party', 'pm_id',
                      self.gf('django.db.models.fields.IntegerField')(blank=True, unique=True, null=True, db_index=True),
                      keep_default=False)

        # Adding field 'Party.pm_prefix'
        db.add_column(u'website_party', 'pm_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Party.pma_prefix'
        db.delete_column(u'website_party', 'pma_prefix')

        # Deleting field 'Party.pma_id'
        db.delete_column(u'website_party', 'pma_id')

        # Deleting field 'Party.pma_ref'
        db.delete_column(u'website_party', 'pma_ref')

        # Adding field 'Dossier.pm_id'
        db.add_column(u'website_dossier', 'pm_id',
                      self.gf('django.db.models.fields.IntegerField')(blank=True, unique=True, null=True, db_index=True),
                      keep_default=False)

        # Adding field 'Dossier.pm_prefix'
        db.add_column(u'website_dossier', 'pm_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Dossier.pma_prefix'
        db.delete_column(u'website_dossier', 'pma_prefix')

        # Deleting field 'Dossier.pma_id'
        db.delete_column(u'website_dossier', 'pma_id')

        # Deleting field 'Dossier.pma_ref'
        db.delete_column(u'website_dossier', 'pma_ref')

        # Adding field 'Politician.pm_id'
        db.add_column(u'website_politician', 'pm_id',
                      self.gf('django.db.models.fields.IntegerField')(blank=True, unique=True, null=True, db_index=True),
                      keep_default=False)

        # Adding field 'Politician.pm_prefix'
        db.add_column(u'website_politician', 'pm_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Politician.pma_prefix'
        db.delete_column(u'website_politician', 'pma_prefix')

        # Deleting field 'Politician.pma_id'
        db.delete_column(u'website_politician', 'pma_id')

        # Deleting field 'Politician.pma_ref'
        db.delete_column(u'website_politician', 'pma_ref')

        # Adding field 'Proceeding.pm_id'
        db.add_column(u'website_proceeding', 'pm_id',
                      self.gf('django.db.models.fields.IntegerField')(blank=True, unique=True, null=True, db_index=True),
                      keep_default=False)

        # Adding field 'Proceeding.pm_prefix'
        db.add_column(u'website_proceeding', 'pm_prefix',
                      self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Proceeding.pma_prefix'
        db.delete_column(u'website_proceeding', 'pma_prefix')

        # Deleting field 'Proceeding.pma_id'
        db.delete_column(u'website_proceeding', 'pma_id')

        # Deleting field 'Proceeding.pma_ref'
        db.delete_column(u'website_proceeding', 'pma_ref')


    models = {
        u'website.dossier': {
            'Meta': {'object_name': 'Dossier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pma_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pma_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pma_ref': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '60', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pma_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pma_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pma_ref': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '60', 'unique': 'True', 'null': 'True', 'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'unique_together': "(['vote', 'party'],)", 'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']", 'null': 'True', 'blank': 'True'}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'ordering': "['name']", 'object_name': 'Politician'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'pma_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pma_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pma_ref': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '60', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'unique_together': "(['file_number', 'file_subnumber'],)", 'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'dossier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            'file_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'file_subnumber': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pma_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'pma_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'pma_ref': ('django.db.models.fields.CharField', [], {'db_index': 'True', 'max_length': '60', 'unique': 'True', 'null': 'True', 'blank': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']", 'null': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']