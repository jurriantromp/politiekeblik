# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting model 'Dossier'
        db.delete_table(u'website_dossier')

        # Adding model 'ProceedingCategory'
        db.create_table(u'website_proceedingcategory', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('category', self.gf('django.db.models.fields.CharField')(max_length=40)),
            ('subcategory', self.gf('django.db.models.fields.CharField')(max_length=40)),
        ))
        db.send_create_signal(u'website', ['ProceedingCategory'])

        # Deleting field 'Proceeding.file_number'
        db.delete_column(u'website_proceeding', 'file_number')

        # Deleting field 'Proceeding.categories'
        db.delete_column(u'website_proceeding', 'categories')

        # Deleting field 'Proceeding.file_subnumber'
        db.delete_column(u'website_proceeding', 'file_subnumber')

        # Deleting field 'Proceeding.dossier'
        db.delete_column(u'website_proceeding', 'dossier_id')

        # Adding field 'Proceeding.dossier_number'
        db.add_column(u'website_proceeding', 'dossier_number',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=14),
                      keep_default=False)

        # Adding field 'Proceeding.dossier_subnumber'
        db.add_column(u'website_proceeding', 'dossier_subnumber',
                      self.gf('django.db.models.fields.CharField')(default=None, max_length=8),
                      keep_default=False)

        # Adding M2M table for field categories on 'Proceeding'
        m2m_table_name = db.shorten_name(u'website_proceeding_categories')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('proceeding', models.ForeignKey(orm[u'website.proceeding'], null=False)),
            ('proceedingcategory', models.ForeignKey(orm[u'website.proceedingcategory'], null=False))
        ))
        db.create_unique(m2m_table_name, ['proceeding_id', 'proceedingcategory_id'])


        # Changing field 'Proceeding.house'
        db.alter_column(u'website_proceeding', 'house', self.gf('django.db.models.fields.IntegerField')())
        # Removing M2M table for field dossiers on 'Commission'
        db.delete_table(db.shorten_name(u'website_commission_dossiers'))


    def backwards(self, orm):
        # Adding model 'Dossier'
        db.create_table(u'website_dossier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
        ))
        db.send_create_signal(u'website', ['Dossier'])

        # Deleting model 'ProceedingCategory'
        db.delete_table(u'website_proceedingcategory')

        # Adding field 'Proceeding.file_number'
        db.add_column(u'website_proceeding', 'file_number',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=14),
                      keep_default=False)

        # Adding field 'Proceeding.categories'
        db.add_column(u'website_proceeding', 'categories',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Proceeding.file_subnumber'
        db.add_column(u'website_proceeding', 'file_subnumber',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=8),
                      keep_default=False)

        # Adding field 'Proceeding.dossier'
        db.add_column(u'website_proceeding', 'dossier',
                      self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Dossier'], null=True, blank=True),
                      keep_default=False)

        # Deleting field 'Proceeding.dossier_number'
        db.delete_column(u'website_proceeding', 'dossier_number')

        # Deleting field 'Proceeding.dossier_subnumber'
        db.delete_column(u'website_proceeding', 'dossier_subnumber')

        # Removing M2M table for field categories on 'Proceeding'
        db.delete_table(db.shorten_name(u'website_proceeding_categories'))


        # Changing field 'Proceeding.house'
        db.alter_column(u'website_proceeding', 'house', self.gf('django.db.models.fields.CharField')(max_length=20))
        # Adding M2M table for field dossiers on 'Commission'
        m2m_table_name = db.shorten_name(u'website_commission_dossiers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('commission', models.ForeignKey(orm[u'website.commission'], null=False)),
            ('dossier', models.ForeignKey(orm[u'website.dossier'], null=False))
        ))
        db.create_unique(m2m_table_name, ['commission_id', 'dossier_id'])


    models = {
        u'website.coalition': {
            'Meta': {'object_name': 'Coalition'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Party']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.commission': {
            'Meta': {'object_name': 'Commission'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Politician']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'hex_color': ('django.db.models.fields.CharField', [], {'max_length': '7', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'logo': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'seats_commons': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'}),
            'seats_senate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'unique_together': "(['vote', 'party'],)", 'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']"}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']"}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'Politician'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'house': ('django.db.models.fields.SmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'photo': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'seniority': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.ProceedingCategory']", 'null': 'True', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {}),
            'dossier_number': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'dossier_subnumber': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'house': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'})
        },
        u'website.proceedingcategory': {
            'Meta': {'object_name': 'ProceedingCategory'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subcategory': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']", 'unique': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']