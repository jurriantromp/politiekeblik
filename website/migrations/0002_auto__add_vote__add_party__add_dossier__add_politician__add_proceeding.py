# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Vote'
        db.create_table(u'website_vote', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('proceeding', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Proceeding'], null=True, blank=True)),
            ('passed', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Vote'])

        # Adding M2M table for field submitted_by on 'Vote'
        m2m_table_name = db.shorten_name(u'website_vote_submitted_by')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('vote', models.ForeignKey(orm[u'website.vote'], null=False)),
            ('politician', models.ForeignKey(orm[u'website.politician'], null=False))
        ))
        db.create_unique(m2m_table_name, ['vote_id', 'politician_id'])

        # Adding model 'Party'
        db.create_table(u'website_party', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pm_prefix', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('pm_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('abbreviation', self.gf('django.db.models.fields.CharField')(max_length=10, null=True, blank=True)),
            ('logo', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Party'])

        # Adding model 'Dossier'
        db.create_table(u'website_dossier', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pm_prefix', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('pm_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Dossier'])

        # Adding model 'Politician'
        db.create_table(u'website_politician', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pm_prefix', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('pm_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('party', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Party'], null=True, blank=True)),
            ('role', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('picture', self.gf('django.db.models.fields.URLField')(max_length=200, null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Politician'])

        # Adding model 'Proceeding'
        db.create_table(u'website_proceeding', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('pm_prefix', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
            ('pm_id', self.gf('django.db.models.fields.IntegerField')(db_index=True, null=True, blank=True)),
            ('proceeding_type', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('legislative_period', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('session_number', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('file_number', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('file_subnumber', self.gf('django.db.models.fields.IntegerField')(null=True, blank=True)),
            ('date_of_vote', self.gf('django.db.models.fields.DateField')(null=True, blank=True)),
            ('categories', self.gf('django.db.models.fields.CharField')(max_length=100, null=True, blank=True)),
            ('dossier', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Dossier'], null=True, blank=True)),
            ('title', self.gf('django.db.models.fields.CharField')(max_length=255, null=True, blank=True)),
            ('house', self.gf('django.db.models.fields.CharField')(max_length=20, null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['Proceeding'])

        # Adding model 'PartyVote'
        db.create_table(u'website_partyvote', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('vote', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Vote'], null=True, blank=True)),
            ('party', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Party'], null=True, blank=True)),
            ('vote_result', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
            ('explicit', self.gf('django.db.models.fields.NullBooleanField')(null=True, blank=True)),
        ))
        db.send_create_signal(u'website', ['PartyVote'])


    def backwards(self, orm):
        # Deleting model 'Vote'
        db.delete_table(u'website_vote')

        # Removing M2M table for field submitted_by on 'Vote'
        db.delete_table(db.shorten_name(u'website_vote_submitted_by'))

        # Deleting model 'Party'
        db.delete_table(u'website_party')

        # Deleting model 'Dossier'
        db.delete_table(u'website_dossier')

        # Deleting model 'Politician'
        db.delete_table(u'website_politician')

        # Deleting model 'Proceeding'
        db.delete_table(u'website_proceeding')

        # Deleting model 'PartyVote'
        db.delete_table(u'website_partyvote')


    models = {
        u'website.dossier': {
            'Meta': {'object_name': 'Dossier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '10', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'logo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']", 'null': 'True', 'blank': 'True'}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'object_name': 'Politician'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'picture': ('django.db.models.fields.URLField', [], {'max_length': '200', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'role': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'dossier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            'file_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'file_subnumber': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.IntegerField', [], {'db_index': 'True', 'null': 'True', 'blank': 'True'}),
            'pm_prefix': ('django.db.models.fields.CharField', [], {'max_length': '20', 'null': 'True', 'blank': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100', 'null': 'True', 'blank': 'True'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {'null': 'True', 'blank': 'True'}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '255', 'null': 'True', 'blank': 'True'})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']", 'null': 'True', 'blank': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']