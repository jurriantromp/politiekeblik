# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Adding model 'Commission'
        db.create_table(u'website_commission', (
            (u'id', self.gf('django.db.models.fields.AutoField')(primary_key=True)),
            ('name', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('subject', self.gf('django.db.models.fields.CharField')(max_length=100)),
            ('type', self.gf('django.db.models.fields.CharField')(max_length=100)),
        ))
        db.send_create_signal(u'website', ['Commission'])

        # Adding M2M table for field members on 'Commission'
        m2m_table_name = db.shorten_name(u'website_commission_members')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('commission', models.ForeignKey(orm[u'website.commission'], null=False)),
            ('politician', models.ForeignKey(orm[u'website.politician'], null=False))
        ))
        db.create_unique(m2m_table_name, ['commission_id', 'politician_id'])

        # Adding M2M table for field dossiers on 'Commission'
        m2m_table_name = db.shorten_name(u'website_commission_dossiers')
        db.create_table(m2m_table_name, (
            ('id', models.AutoField(verbose_name='ID', primary_key=True, auto_created=True)),
            ('commission', models.ForeignKey(orm[u'website.commission'], null=False)),
            ('dossier', models.ForeignKey(orm[u'website.dossier'], null=False))
        ))
        db.create_unique(m2m_table_name, ['commission_id', 'dossier_id'])

        # Adding unique constraint on 'Vote', fields ['proceeding']
        db.create_unique(u'website_vote', ['proceeding_id'])

        # Deleting field 'Dossier.pm_id'
        db.delete_column(u'website_dossier', 'pm_id')

        # Adding field 'Politician.house'
        db.add_column(u'website_politician', 'house',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=20),
                      keep_default=False)


    def backwards(self, orm):
        # Removing unique constraint on 'Vote', fields ['proceeding']
        db.delete_unique(u'website_vote', ['proceeding_id'])

        # Deleting model 'Commission'
        db.delete_table(u'website_commission')

        # Removing M2M table for field members on 'Commission'
        db.delete_table(db.shorten_name(u'website_commission_members'))

        # Removing M2M table for field dossiers on 'Commission'
        db.delete_table(db.shorten_name(u'website_commission_dossiers'))

        # Adding field 'Dossier.pm_id'
        db.add_column(u'website_dossier', 'pm_id',
                      self.gf('django.db.models.fields.CharField')(default=0, max_length=50, unique=True, db_index=True),
                      keep_default=False)

        # Deleting field 'Politician.house'
        db.delete_column(u'website_politician', 'house')


    models = {
        u'website.coalition': {
            'Meta': {'object_name': 'Coalition'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Party']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.commission': {
            'Meta': {'object_name': 'Commission'},
            'dossiers': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Politician']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'website.dossier': {
            'Meta': {'object_name': 'Dossier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'hex_color': ('django.db.models.fields.CharField', [], {'max_length': '7', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'logo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'seats_commons': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'}),
            'seats_senate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'unique_together': "(['vote', 'party'],)", 'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']"}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']"}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'Politician'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']"}),
            'photo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'object_name': 'Proceeding'},
            '_title': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'}),
            'categories': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {}),
            'dossier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            'file_number': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'file_subnumber': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']", 'unique': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']