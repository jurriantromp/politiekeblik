# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):

        # Changing field 'Vote.proceeding'
        db.alter_column(u'website_vote', 'proceeding_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['website.Proceeding']))

        # Changing field 'Vote.passed'
        db.alter_column(u'website_vote', 'passed', self.gf('django.db.models.fields.BooleanField')())

        # Changing field 'Party.name'
        db.alter_column(u'website_party', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Party.abbreviation'
        db.alter_column(u'website_party', 'abbreviation', self.gf('django.db.models.fields.CharField')(default='', max_length=40))

        # Changing field 'Politician.first_name'
        db.alter_column(u'website_politician', 'first_name', self.gf('django.db.models.fields.CharField')(default='', max_length=40))

        # Changing field 'Politician.last_name'
        db.alter_column(u'website_politician', 'last_name', self.gf('django.db.models.fields.CharField')(default='', max_length=40))

        # Changing field 'Politician.photo'
        db.alter_column(u'website_politician', 'photo', self.gf('django.db.models.fields.URLField')(default='', max_length=200))

        # Changing field 'Politician.party'
        db.alter_column(u'website_politician', 'party_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['website.Party']))

        # Changing field 'Coalition.name'
        db.alter_column(u'website_coalition', 'name', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Proceeding.title'
        db.alter_column(u'website_proceeding', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=600, blank=True))


        # Changing field 'Proceeding.session_number'
        db.alter_column(u'website_proceeding', 'session_number', self.gf('django.db.models.fields.IntegerField')(default=1))

        # Changing field 'Proceeding.file_subnumber'
        db.alter_column(u'website_proceeding', 'file_subnumber', self.gf('django.db.models.fields.CharField')(default='', max_length=8))

        # Changing field 'Proceeding.file_number'
        db.alter_column(u'website_proceeding', 'file_number', self.gf('django.db.models.fields.CharField')(default='', max_length=14))

        # Changing field 'Proceeding.date_of_vote'
        db.alter_column(u'website_proceeding', 'date_of_vote', self.gf('django.db.models.fields.DateField')(default=datetime.datetime(2000, 1, 1, 0, 0)))

        # Changing field 'Proceeding.house'
        db.alter_column(u'website_proceeding', 'house', self.gf('django.db.models.fields.CharField')(default='', max_length=20))

        # Changing field 'Proceeding.proceeding_type'
        db.alter_column(u'website_proceeding', 'proceeding_type', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Proceeding.legislative_period'
        db.alter_column(u'website_proceeding', 'legislative_period', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'Proceeding.categories'
        db.alter_column(u'website_proceeding', 'categories', self.gf('django.db.models.fields.CharField')(default='', max_length=100))

        # Changing field 'PartyVote.vote'
        db.alter_column(u'website_partyvote', 'vote_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['website.Vote']))

        # Changing field 'PartyVote.party'
        db.alter_column(u'website_partyvote', 'party_id', self.gf('django.db.models.fields.related.ForeignKey')(default=1, to=orm['website.Party']))

    def backwards(self, orm):

        # Changing field 'Vote.proceeding'
        db.alter_column(u'website_vote', 'proceeding_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Proceeding'], null=True))

        # Changing field 'Vote.passed'
        db.alter_column(u'website_vote', 'passed', self.gf('django.db.models.fields.NullBooleanField')(null=True))

        # Changing field 'Party.name'
        db.alter_column(u'website_party', 'name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Party.abbreviation'
        db.alter_column(u'website_party', 'abbreviation', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Politician.first_name'
        db.alter_column(u'website_politician', 'first_name', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Politician.last_name'
        db.alter_column(u'website_politician', 'last_name', self.gf('django.db.models.fields.CharField')(max_length=40, null=True))

        # Changing field 'Politician.photo'
        db.alter_column(u'website_politician', 'photo', self.gf('django.db.models.fields.URLField')(max_length=200, null=True))

        # Changing field 'Politician.party'
        db.alter_column(u'website_politician', 'party_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Party'], null=True))

        # Changing field 'Coalition.name'
        db.alter_column(u'website_coalition', 'name', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))
        # Adding field 'Proceeding.title'
        db.alter_column(u'website_proceeding', 'title',
                      self.gf('django.db.models.fields.CharField')(max_length=600, null=True, blank=True)
                      )


        # Changing field 'Proceeding.session_number'
        db.alter_column(u'website_proceeding', 'session_number', self.gf('django.db.models.fields.IntegerField')(null=True))

        # Changing field 'Proceeding.file_subnumber'
        db.alter_column(u'website_proceeding', 'file_subnumber', self.gf('django.db.models.fields.CharField')(max_length=8, null=True))

        # Changing field 'Proceeding.file_number'
        db.alter_column(u'website_proceeding', 'file_number', self.gf('django.db.models.fields.CharField')(max_length=14, null=True))

        # Changing field 'Proceeding.date_of_vote'
        db.alter_column(u'website_proceeding', 'date_of_vote', self.gf('django.db.models.fields.DateField')(null=True))

        # Changing field 'Proceeding.house'
        db.alter_column(u'website_proceeding', 'house', self.gf('django.db.models.fields.CharField')(max_length=20, null=True))

        # Changing field 'Proceeding.proceeding_type'
        db.alter_column(u'website_proceeding', 'proceeding_type', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Proceeding.legislative_period'
        db.alter_column(u'website_proceeding', 'legislative_period', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'Proceeding.categories'
        db.alter_column(u'website_proceeding', 'categories', self.gf('django.db.models.fields.CharField')(max_length=100, null=True))

        # Changing field 'PartyVote.vote'
        db.alter_column(u'website_partyvote', 'vote_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Vote'], null=True))

        # Changing field 'PartyVote.party'
        db.alter_column(u'website_partyvote', 'party_id', self.gf('django.db.models.fields.related.ForeignKey')(to=orm['website.Party'], null=True))

    models = {
        u'website.coalition': {
            'Meta': {'object_name': 'Coalition'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Party']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.dossier': {
            'Meta': {'object_name': 'Dossier'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'hex_color': ('django.db.models.fields.CharField', [], {'max_length': '7', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'logo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'seats_commons': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'}),
            'seats_senate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'unique_together': "(['vote', 'party'],)", 'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']"}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']"}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'Politician'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']"}),
            'photo': ('django.db.models.fields.URLField', [], {'max_length': '200', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {}),
            'dossier': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Dossier']", 'null': 'True', 'blank': 'True'}),
            'file_number': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'file_subnumber': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'house': ('django.db.models.fields.CharField', [], {'max_length': '20'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'proceeding': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Proceeding']"}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']