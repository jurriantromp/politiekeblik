# -*- coding: utf-8 -*-
import datetime
from south.db import db
from south.v2 import SchemaMigration
from django.db import models


class Migration(SchemaMigration):

    def forwards(self, orm):
        # Deleting field 'Party.logo'
        db.delete_column(u'website_party', 'logo')

        # Adding field 'Party.image'
        db.add_column(u'website_party', 'image',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Party.rendered_circle'
        db.add_column(u'website_party', 'rendered_circle',
                      self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Politician.photo'
        db.delete_column(u'website_politician', 'photo')

        # Deleting field 'Politician.rendered_photo'
        db.delete_column(u'website_politician', 'rendered_photo')

        # Adding field 'Politician.image'
        db.add_column(u'website_politician', 'image',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Politician.rendered_circle'
        db.add_column(u'website_politician', 'rendered_circle',
                      self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True),
                      keep_default=False)


    def backwards(self, orm):
        # Adding field 'Party.logo'
        db.add_column(u'website_party', 'logo',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Party.image'
        db.delete_column(u'website_party', 'image')

        # Deleting field 'Party.rendered_circle'
        db.delete_column(u'website_party', 'rendered_circle')

        # Adding field 'Politician.photo'
        db.add_column(u'website_politician', 'photo',
                      self.gf('django.db.models.fields.CharField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Adding field 'Politician.rendered_photo'
        db.add_column(u'website_politician', 'rendered_photo',
                      self.gf('django.db.models.fields.files.ImageField')(default='', max_length=100, blank=True),
                      keep_default=False)

        # Deleting field 'Politician.image'
        db.delete_column(u'website_politician', 'image')

        # Deleting field 'Politician.rendered_circle'
        db.delete_column(u'website_politician', 'rendered_circle')


    models = {
        u'website.coalition': {
            'Meta': {'object_name': 'Coalition'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'party': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Party']", 'symmetrical': 'False'}),
            'type': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.commission': {
            'Meta': {'object_name': 'Commission'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'members': ('django.db.models.fields.related.ManyToManyField', [], {'to': u"orm['website.Politician']", 'symmetrical': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'subject': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'type': ('django.db.models.fields.CharField', [], {'max_length': '100'})
        },
        u'website.party': {
            'Meta': {'object_name': 'Party'},
            'abbreviation': ('django.db.models.fields.CharField', [], {'max_length': '40', 'blank': 'True'}),
            'hex_color': ('django.db.models.fields.CharField', [], {'max_length': '7', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'name': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'rendered_circle': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'seats_commons': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'}),
            'seats_senate': ('django.db.models.fields.PositiveSmallIntegerField', [], {'blank': 'True'})
        },
        u'website.partyvote': {
            'Meta': {'unique_together': "(['vote', 'party'],)", 'object_name': 'PartyVote'},
            'explicit': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']"}),
            'vote': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Vote']"}),
            'vote_result': ('django.db.models.fields.NullBooleanField', [], {'null': 'True', 'blank': 'True'})
        },
        u'website.politician': {
            'Meta': {'ordering': "['last_name']", 'object_name': 'Politician'},
            'first_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'house': ('django.db.models.fields.SmallIntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'image': ('django.db.models.fields.CharField', [], {'max_length': '100', 'blank': 'True'}),
            'is_active': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'last_name': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            'party': ('django.db.models.fields.related.ForeignKey', [], {'to': u"orm['website.Party']", 'null': 'True', 'blank': 'True'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'rendered_circle': ('django.db.models.fields.files.ImageField', [], {'max_length': '100', 'blank': 'True'}),
            'seniority': ('django.db.models.fields.DateField', [], {'null': 'True', 'blank': 'True'}),
            'twitter': ('django.db.models.fields.CharField', [], {'max_length': '15', 'blank': 'True'})
        },
        u'website.proceeding': {
            'Meta': {'object_name': 'Proceeding'},
            'categories': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.ProceedingCategory']", 'null': 'True', 'blank': 'True'}),
            'date_of_vote': ('django.db.models.fields.DateField', [], {}),
            'dossier_number': ('django.db.models.fields.CharField', [], {'max_length': '14'}),
            'dossier_subnumber': ('django.db.models.fields.CharField', [], {'max_length': '8'}),
            'house': ('django.db.models.fields.IntegerField', [], {}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'legislative_period': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'pm_id': ('django.db.models.fields.CharField', [], {'unique': 'True', 'max_length': '50', 'db_index': 'True'}),
            'proceeding_type': ('django.db.models.fields.CharField', [], {'max_length': '100'}),
            'session_number': ('django.db.models.fields.IntegerField', [], {}),
            'title': ('django.db.models.fields.CharField', [], {'max_length': '600', 'blank': 'True'})
        },
        u'website.proceedingcategory': {
            'Meta': {'object_name': 'ProceedingCategory'},
            'category': ('django.db.models.fields.CharField', [], {'max_length': '40'}),
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'subcategory': ('django.db.models.fields.CharField', [], {'max_length': '40'})
        },
        u'website.vote': {
            'Meta': {'object_name': 'Vote'},
            u'id': ('django.db.models.fields.AutoField', [], {'primary_key': 'True'}),
            'party_votes': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Party']", 'null': 'True', 'through': u"orm['website.PartyVote']", 'blank': 'True'}),
            'passed': ('django.db.models.fields.BooleanField', [], {'default': 'False'}),
            'proceeding': ('django.db.models.fields.related.OneToOneField', [], {'to': u"orm['website.Proceeding']", 'unique': 'True'}),
            'submitted_by': ('django.db.models.fields.related.ManyToManyField', [], {'symmetrical': 'False', 'to': u"orm['website.Politician']", 'null': 'True', 'blank': 'True'})
        }
    }

    complete_apps = ['website']