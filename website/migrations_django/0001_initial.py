# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from django.db import models, migrations
import website.storage


class Migration(migrations.Migration):

    dependencies = [
    ]

    operations = [
        migrations.CreateModel(
            name='Coalition',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100, blank=True)),
                ('type', models.NullBooleanField(choices=[(0, b'Oppositie'), (1, b'Regering')])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Commission',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('name', models.CharField(max_length=100)),
                ('subject', models.CharField(max_length=100)),
                ('type', models.CharField(max_length=100)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Party',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pm_id', models.CharField(help_text=b'PoliticalMashup id', unique=True, max_length=50, db_index=True)),
                ('name', models.CharField(max_length=100)),
                ('abbreviation', models.CharField(max_length=40, blank=True)),
                ('seats_commons', models.PositiveSmallIntegerField(blank=True)),
                ('seats_senate', models.PositiveSmallIntegerField(blank=True)),
                ('image', models.CharField(max_length=100, blank=True)),
                ('rendered_circle', models.ImageField(storage=website.storage.OverwriteStorage(), upload_to=b'rendered_circles', blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('hex_color', models.CharField(max_length=7, blank=True)),
            ],
            options={
                'verbose_name': 'Party',
                'verbose_name_plural': 'Parties',
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='PartyVote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('vote_result', models.NullBooleanField()),
                ('explicit', models.NullBooleanField()),
                ('party', models.ForeignKey(to='website.Party')),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Politician',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pm_id', models.CharField(help_text=b'PoliticalMashup id', unique=True, max_length=50, db_index=True)),
                ('first_name', models.CharField(max_length=40)),
                ('last_name', models.CharField(max_length=40)),
                ('image', models.CharField(max_length=100, blank=True)),
                ('rendered_circle', models.ImageField(storage=website.storage.OverwriteStorage(), upload_to=b'rendered_circles', blank=True)),
                ('house', models.SmallIntegerField(choices=[(0, b'Eerste kamer'), (1, b'Tweede kamer'), (2, b'Regering')])),
                ('seniority', models.DateField(null=True, blank=True)),
                ('twitter', models.CharField(help_text=b'Zonder @', max_length=15, blank=True)),
                ('is_active', models.BooleanField(default=True)),
                ('party', models.ForeignKey(blank=True, to='website.Party', null=True)),
            ],
            options={
                'ordering': ['last_name'],
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Proceeding',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('pm_id', models.CharField(help_text=b'PoliticalMashup id', unique=True, max_length=50, db_index=True)),
                ('proceeding_type', models.CharField(max_length=100)),
                ('legislative_period', models.CharField(max_length=100)),
                ('session_number', models.IntegerField()),
                ('dossier_number', models.CharField(max_length=14)),
                ('dossier_subnumber', models.CharField(max_length=8)),
                ('date_of_vote', models.DateField()),
                ('title', models.CharField(max_length=600, blank=True)),
                ('house', models.IntegerField(choices=[(b'Eerste kamer', 0), (b'Tweede kamer', 1)])),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='ProceedingCategory',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('category', models.CharField(max_length=40)),
                ('subcategory', models.CharField(max_length=40)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.CreateModel(
            name='Vote',
            fields=[
                ('id', models.AutoField(verbose_name='ID', serialize=False, auto_created=True, primary_key=True)),
                ('passed', models.BooleanField(default=True)),
                ('party_votes', models.ManyToManyField(to='website.Party', null=True, through='website.PartyVote', blank=True)),
                ('proceeding', models.OneToOneField(to='website.Proceeding')),
                ('submitted_by', models.ManyToManyField(to='website.Politician', null=True, blank=True)),
            ],
            options={
            },
            bases=(models.Model,),
        ),
        migrations.AddField(
            model_name='proceeding',
            name='categories',
            field=models.ManyToManyField(to='website.ProceedingCategory', null=True, blank=True),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='partyvote',
            name='vote',
            field=models.ForeignKey(to='website.Vote'),
            preserve_default=True,
        ),
        migrations.AlterUniqueTogether(
            name='partyvote',
            unique_together=set([('vote', 'party')]),
        ),
        migrations.AddField(
            model_name='commission',
            name='members',
            field=models.ManyToManyField(to='website.Politician'),
            preserve_default=True,
        ),
        migrations.AddField(
            model_name='coalition',
            name='party',
            field=models.ManyToManyField(to='website.Party'),
            preserve_default=True,
        ),
    ]
