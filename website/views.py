from django.http import HttpResponse, HttpResponseRedirect, Http404
from django.shortcuts import render, get_object_or_404
from website.models import Politician, PartyVote, Vote, Proceeding, Party, Coalition
from website.decorators import json_view
import json
from django.db.models import Max, Count
from datetime import datetime
import time


def list_politicians(kamer=None):

        where = ''
        if kamer == 'tweedekamer':
            where = 'WHERE po.house = 1'
        elif kamer == 'eerstekamer':
            where = 'WHERE po.house = 0'
        elif kamer == 'regering':
            where = 'WHERE po.house = 2'

        query = """
        SELECT po.id, pa.pm_id AS pa_pm_id, pa.name AS pa_name, pa.image AS pa_image, po.first_name, po.last_name, po.pm_id, po.rendered_circle, SUM(IF(vo.passed = 1,1,0)) / COUNT(vo.id) * 100 AS percent, COUNT(vo.id) AS total
        FROM website_politician AS po
        JOIN website_party AS pa ON po.party_id = pa.id
        LEFT JOIN website_vote_submitted_by AS vs ON vs.politician_id = po.id
        LEFT JOIN website_vote AS vo ON vs.vote_id = vo.id
        """ + where + """
        GROUP BY po.id
        ORDER BY pa.abbreviation, percent DESC, total DESC, po.last_name
        """

        results = dict()
        for politician in Politician.objects.raw(query):
            if not politician.pa_pm_id in results:
                results[politician.pa_pm_id] = dict()
                results[politician.pa_pm_id]['name'] = politician.pa_name
                results[politician.pa_pm_id]['image'] = politician.pa_image
                results[politician.pa_pm_id]['politicians'] = list()
            results[politician.pa_pm_id]['politicians'].append(politician)  # ([politician.pm_id, politician.first_name, politician.last_name, politician.percent, politician.total])
        return results


def homepage(request):
    #politician = Politician.active.filter(vote__submitted_by__isnull=False).order_by('?')[0]
    #parties = Party.active.all()

    party_politicians = list_politicians()

    return render(request, 'politician.html', {'party_politicians': party_politicians})


def eerstekamer(request):
    #politician = Politician.active.filter(vote__submitted_by__isnull=False).order_by('?')[0]
    #parties = Party.active.all()

    party_politicians = list_politicians('eerstekamer')

    return render(request, 'politician.html', {'party_politicians': party_politicians})


def tweedekamer(request):
    #politician = Politician.active.filter(vote__submitted_by__isnull=False).order_by('?')[0]
    #parties = Party.active.all()

    party_politicians = list_politicians('tweedekamer')

    return render(request, 'politician.html', {'party_politicians': party_politicians})


def regering(request):
    #politician = Politician.active.filter(vote__submitted_by__isnull=False).order_by('?')[0]
    #parties = Party.active.all()

    party_politicians = list_politicians('regering')

    return render(request, 'politician.html', {'party_politicians': party_politicians})


def politician(request, pm_id):
    politician = Politician.active.get(pm_id=pm_id)

    submitted = politician.vote_set.all()

    voted = PartyVote.objects.filter(party=politician.party)

    return render(request, 'ajax/politician.html', {'politician': politician, 'voted': voted, 'submitted': submitted})


def party(request, pm_id=None):
    if pm_id:
        politicians = Politician.active.filter(party__pm_id=pm_id).order_by('last_name')
        return render(request, 'homepage.html', {'politicians': politicians})
    else:
        parties = Party.active.all()
        return render(request, 'parties.html', {'parties': parties})


def government(request):
    politicians = Politician.active.filter(house=2)
    return render(request, 'government.html', {'politicians': politicians})


def proceeding(request, pm_id):
    proceeding = Proceeding.objects.get(pm_id=pm_id)
    vote = Vote.objects.get(proceeding=proceeding)
    return render(request, 'proceeding.html', {'proceeding': proceeding, 'vote': vote})


def compare(request, pm_id1, pm_id2):
    print [pm_id1, pm_id2]
    politicians = Politician.active.filter(pm_id__in=[pm_id1, pm_id2])
    print politicians
    return render(request, 'compare.html', {'politicians': politicians})


def ajax_resolver(request, pm_id):
    identifiers = pm_id.split(".")
    if pm_id[:5] == "nl.m.":
        politician = Politician.active.get(pm_id=pm_id)
        submitted = politician.vote_set.filter(proceeding__isnull=False)
        voted = PartyVote.objects.filter(party=politician.party, vote__proceeding__isnull=False)[:30]
        return render(request, 'ajax/politician.html', {'politician': politician, 'voted': voted, 'submitted': submitted, 'identifiers': identifiers})
    elif pm_id[:5] == "nl.p.":
        party = Party.active.get(pm_id=pm_id)
        politicians = party.politician_set.all()
        for politician in politicians:
            politician.identifiers = politician.pm_id.split(".")
        return render(request, 'party.html', {'party': party, 'identifiers': identifiers, 'politicians': politicians})
    elif pm_id[:14] == 'nl.proc.ob.d.h':
        return proceeding(request, pm_id)


#@json_view()
def bubbles_json(request, view=None):
    data = list()
    if view == 'coalitions':
        for coalition in Coalition.objects.all():
            c = list()
            for party in coalition.party.all():
                d = list()

                po = dict()
                po["name"] = party.abbreviation
                po["color"] = party.hex_color
                po["value"] = 50
                po["image"] = party.image
                po["pmid"] = party.pm_id

                d.append(po) #///////////

                for politician in party.politician_set.all():
                    if politician.submit_count()['all'] > 1:
                        co = dict()
                        co["name"] = politician.name
                        co["color"] = politician.party.hex_color
                        co["value"] = politician.submit_count()['all']
                        co["image"] = politician.image
                        co["pmid"] = politician.pm_id

                        d.append(co)

                p = dict()
                p["name"] = party.pm_id
                p["children"] = d

                c.append(p)

            g = dict()
            g["name"] = coalition.title
            g["children"] = c

            data.append(g)
    elif view == 'mixed':
        #c = list()
        for party in Party.active.all():
            for politician in party.politician_set.all():
                if politician.submit_count()['all'] > 1:
                    co = dict()
                    co["name"] = politician.name
                    co["color"] = politician.party.hex_color
                    co["value"] = politician.submit_count()['all']
                    co["image"] = politician.image
                    co["pmid"] = politician.pm_id

                    data.append(co)

    else:
        for party in Party.active.all():
            if party.politician_set.count():
                d = list()

                po = dict()
                po["name"] = party.abbreviation
                po["color"] = party.hex_color
                po["value"] = 40
                po["image"] = party.image
                po["pmid"] = party.pm_id

                d.append(po) #///////////

                for politician in party.politician_set.all():
                    if politician.submit_count()['all'] > 4 if view == "percent" else politician.submit_count()['all'] > 1:
                        co = dict()
                        co["name"] = politician.name
                        co["color"] = politician.party.hex_color
                        if view == 'moties':
                            co["value"] = politician.submit_count()['moties']
                        elif view == 'support':
                            co["value"] = politician.submit_support()['absolute']['Partij voor de Vrijheid'][0]
                            #print politician.submit_support()['relative']['Partij voor de Vrijheid'][0]
                        elif view == 'percent':
                            co["value"] = politician.submit_score()['percent']
                        else:
                            co["value"] = politician.submit_count()['all']
                        co["image"] = politician.image
                        co["pmid"] = politician.pm_id

                        d.append(co)

                p = dict()
                p["name"] = party.pm_id
                p["children"] = d

                data.append(p)

    #return {'result': json}
    return HttpResponse(json.dumps({"children": data}), content_type='application/json')


#@json_view()
def politician_json(request, pm_id=None):
    if pm_id:
        politician = Politician.active.get(pm_id=pm_id)
    else:
        politician = Politician.active.filter(vote__submitted_by__isnull=False).order_by('?')[0]

    data = dict()
    data["id"] = politician.id
    data["name"] = politician.name
    data["party_image"] = politician.party.image
    data["image"] = politician.image
    data["house"] = politician.get_house_display()
    #data["twitter"] = politician.twitter #todo implement twitter

    #delta = date.today - politician.seniority #todo implement seniority
    #data["seniority"] = delta.days()
    #data["seniority"] = 2000
    #data["most_senior"] = 3000 #todo senior pm_id and logo for hover

    data["statistics"] = politician.submit_statistics()

    #data["portfolio"] #todo implement portfolio

    support = politician.submit_support()
    data["support"] = support
    data["categories"] = politician.category_support()

    recent = list()
    for item in politician.vote_set.all().values('proceeding__title', 'proceeding__pm_id', 'passed').order_by('-proceeding__date_of_vote')[:5]:
        passed = 'passed' if item['passed'] else 'failed'
        recent.append([item['proceeding__title'][:60], '/resolver/' + item['proceeding__pm_id'], passed])

    data["recent"] = recent

    parties = []
    aye = []
    no = []
    for party in support:
        parties.append(party[0].upper())
        aye.append(float(party[2]))
        no.append(float(0 - party[3]))
    data["data"] = [parties, aye, no]

    #passed = list()
    #failed = list()
    #for submission in politician.vote_set.values('passed', 'proceeding__date_of_vote').annotate(vote_count=Count('proceeding__date_of_vote')).order_by('proceeding__date_of_vote'): #todo create (cached?) function
    #    dt = datetime.combine(submission['proceeding__date_of_vote'], datetime.min.time())
    #    timestamp = time.mktime(dt.timetuple()) + dt.microsecond / 1000000.0
    #    tstr = int(str(timestamp)[:-2] + '000') #todo temp timestamp
    #
    #    if submission['passed'] == True: #explicit
    #        passed.append((tstr, submission['vote_count']))#, submission.proceeding.pm_id))
    #    elif submission['passed'] == False:
    #        failed.append((tstr, submission['vote_count']))#, submission.proceeding.pm_id))
    #data["submissions"] = {'passed': passed, 'failed': failed}

    #data["related"] #todo implement related

    return HttpResponse(json.dumps(data), content_type='application/json')


def party_json(request, pm_id=None):
    if pm_id:
        party = Party.active.get(pm_id=pm_id)
    else:
        party = Party.active.filter(vote__submitted_by__isnull=False).order_by('?')[0]

    data = dict()
    data["id"] = party.id
    data["name"] = party.name
    data["abbreviation"] = party.abbreviation
    data["party_image"] = party.image
    data["seats_commons"] = party.seats_commons
    data["seats_senate"] = party.seats_senate
    #data["twitter"] = politician.twitter #todo implement twitter

    data["statistics"] = party.submit_statistics()

    support = party.submit_support()
    data["support"] = support
    data["categories"] = party.category_support()

    recent = list()
    for item in Proceeding.objects.filter(vote__submitted_by__party=party).values('title', 'pm_id', 'vote__passed').order_by('-date_of_vote')[:5]:
        passed = 'passed' if item['vote__passed'] else 'failed'
        recent.append([item['title'][:60], '/resolver/' + item['pm_id'], passed])

    data["recent"] = recent

    parties = []
    aye = []
    no = []
    for party in support:
        parties.append(party[0].upper())
        aye.append(float(party[2]))
        no.append(float(0 - party[3]))
    data["data"] = [parties, aye, no]

    return HttpResponse(json.dumps(data), content_type='application/json')