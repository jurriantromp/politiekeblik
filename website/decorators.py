import logging
from django.utils.html import escape
from functools import wraps
from django.http import HttpResponse, HttpResponseRedirect
import json
from django.utils.translation import ugettext as _
from django.http import HttpResponseBadRequest
from django.conf import settings
from django.core.cache import cache
from hashlib import sha224

#_log = logging.getLogger(__name__)


def method_cache(seconds=0):
    """
    Modified version from Django snippets: http://djangosnippets.org/snippets/2477/

    A `seconds` value of `0` means that we will not memcache it.

    If a result is cached on instance, return that first.  If that fails, check
    memcached. If all else fails, hit the db and cache on instance and in memcache.

    ** NOTE: Methods that return None are always "recached".
    """

    def inner_cache(method):

        def x(*args, **kwargs):
            key = sha224(str(method.__module__) + str(method.__name__) + \
                str(args) + str(kwargs)).hexdigest()

            result = cache.get(key)

            if result is None:
                # all caches failed, call the actual method
                result = method(*args, **kwargs)

                # save to memcache and class attr
                if seconds and isinstance(seconds, int):
                    cache.set(key, result, seconds)
            return result
        return x
    return inner_cache


def json_view(function=None, redirect_to=None, allow_non_ajax_post=False):
    """
    This decorator expects the requests to be ajax requests (except in
    the case of allow_non_ajax_post=True), and returns a BadRequest
    header (400) when normal requests are encountered.

    The wrapped view should return a dict, which is serialized to
    json and returned as a json response.

    If redirect_to is not None, it is used to redirect to in case of
    a non-ajax request instead of a BadRequest.

    If allow_non_ajax_post is True, POST requests need not be ajax.
    If a POST request is not ajax, the resulting dict is serialized to
    json, wrapped in <textarea> tags and sent in a normal response.
    This is to handle file uploads which use hidden iframes.
    See http://jquery.malsup.com/form/#file-upload

    If cookies should be set on the response, they can be passed in
    with the _cookies key in the response dict.

    Based on a snippet:
        http://www.djangosnippets.org/snippets/622/
    """
    def decorator(function):
        def wrapped(request, *args, **kwargs):
            is_file_upload = False
            if not request.is_ajax():
                if request.method == 'POST' and allow_non_ajax_post:
                    is_file_upload = True
                elif redirect_to:
                    return HttpResponseRedirect(redirect_to)
                else:
                    return HttpResponseBadRequest('Json view expected XMLHttpRequest header')
            response = None
            try:
                response = function(request, *args, **kwargs)
                if not isinstance(response, dict):
                    #_log.error('%s is not of type dict' % response)
                    response = {'result':'error'}
                elif 'result' not in response:
                    response['result'] = 'ok'

            except:
                #_log.exception('Exception when executing json_view for request %s', request.get_full_path())
                # Come what may, we're returning JSON.
                if settings.DEBUG:
                    msg = _('Internal error')+': '+str(args)
                    response = {'result': 'error', 'text': msg}
                else:
                    response = {'result': 'error'}
            json_response = json.dumps(response)

            if is_file_upload:
                httpresponse =  HttpResponse('<textarea>%s</textarea>' % escape(json_response))
            else:
                httpresponse =  HttpResponse(json_response, mimetype='application/json')

            httpresponse.original_dict = response
            for cookie in response.get('_cookies', []):
                httpresponse.set_cookie(**cookie)
            return httpresponse
        return wraps(function)(wrapped)

    if function:
        return decorator(function)
    return decorator