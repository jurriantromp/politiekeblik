from lxml import etree
import urllib2
import csv


def download_xml(url):
    req = urllib2.Request(url)
    opener = urllib2.build_opener()
    try:
        f = opener.open(req)
    except:
        raise Exception("url not found: %s" % url)

    return etree.parse(f)


def download_csv(url):
    req = urllib2.Request(url)
    opener = urllib2.build_opener()
    try:
        f = opener.open(req)
    except:
        raise Exception("url not found: %s" % url)

    return csv.DictReader(url, delimiter=';')