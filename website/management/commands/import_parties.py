from django.core.management.base import LabelCommand
from website.models import *
import csv
from lxml import etree
from django.db import IntegrityError
import urllib2
from django.conf import settings
from sorl.thumbnail import get_thumbnail

class Command(LabelCommand):
    help = 'Test'

    def download_xml(self, url):
        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        try:
            f = opener.open(req)
        except:
            raise Exception("url not found: %s" % url)

        return etree.parse(f)

    def download_csv(self, url):
        req = urllib2.Request(url)
        opener = urllib2.build_opener()
        try:
            f = opener.open(req)
        except:
            raise Exception("url not found: %s" % url)

        csv_data = csv.reader(f, delimiter=';')

        csv_data.next() # todo: fix this
        csv_data.next()
        csv_data.next()

        return csv_data

    def handle(self, *args, **options):
        print "Running..."

        error = list()

        print "Downloading xml... This may take a while."
        party_list = self.download_xml(settings.PM_PARTIES)
        ns = {'pm': 'http://www.politicalmashup.nl'}

        for party in party_list.xpath('//pm:party', namespaces=ns):
            pm_id = party.xpath('string(@pm:id)', namespaces=ns)
            xml = self.download_xml(settings.PM_RESOLVER + pm_id)

            d = dict()
            d['name'] = unicode(xml.xpath('string(pm:party/pm:name/text())', namespaces=ns))

            #photo_url = xml.xpath('string(//pm:link[@pm:linktype="logo"]/text())', namespaces=ns) #todo: temp disabled
            #if photo_url:
            #    filepath = os.path.join("media", "parties", os.path.basename(photo_url))
            #    urllib.urlretrieve(photo_url, filepath)
            #    d['image'] = filepath

            d['abbreviation'] = pm_id.split('.')[-1]
            # d['abbreviation'] = xml.xpath("//pm:name[@pm:nametype='normalised']/text()", namespaces=ns)
            seats_commons = party.xpath('string(pm:seats/pm:session[@pm:house="commons"]/@pm:seats)', namespaces=ns)
            d['seats_commons'] = int(seats_commons) if seats_commons else 0
            seats_senate = party.xpath('string(pm:seats/pm:session[@pm:house="senate"]/@pm:seats)', namespaces=ns)
            d['seats_senate'] = int(seats_senate) if seats_senate else 0
            # d['pm_source'] = r[1]
            d['is_active'] = True

            try:
                party, created = Party.objects.get_or_create(pm_id=pm_id, defaults=d)

                if not created:
                    Party.objects.filter(pk=party.id).update(**d)

                print '%s - created: %s' % (pm_id, created)
            except IntegrityError:
                error.append(pm_id)

        print '%s were ignored' % ' ,'.join(error) or 0