from django.core.management.base import LabelCommand
from django.core.files.base import ContentFile
from website.models import Politician, Party
from django.conf import settings
import os
import urllib2
import re
import json
import base64


service_url = 'http://127.0.0.1:8190/'
source_site = 'http://www.politiekeblik.nl'
pat = re.compile(r'\n\s*')
infile_source = open(os.path.join(settings.BASE_PATH, 'website', 'static', 'js', 'highcharts_infile.json')).read()
callback_source = open(os.path.join(settings.BASE_PATH, 'website', 'static', 'js', 'highcharts_callback.js')).read()
headers = {'Content-Type': 'application/json'}


class Command(LabelCommand):
    help = 'Test'

    def handle(self, *args, **options):
        print "Running..."

        print "Rendering politicians"
        for politician in Politician.objects.all():
            render(politician)
        print

        print "Rendering parties"
        for party in Party.objects.all():
            render(party)


def render(obj):
    infile = infile_source % {"amount": obj.submit_statistics()[3]}
    infile = json.dumps(pat.sub('', infile))

    print obj.image

    callback = callback_source % {"url": source_site + obj.image}
    callback = json.dumps(pat.sub('', callback))

    data = '{"infile":' + infile + ', "callback":' + callback + '}'

    req = urllib2.Request(service_url, data, headers)
    opener = urllib2.build_opener()
    try:
        f = opener.open(req)
    except:
        raise Exception("url not found: %s" % service_url)

    image = base64.b64decode(f.read())
    #print image
    obj.rendered_circle.save('%s_%s.png' % (obj.name, obj.id), ContentFile(image))