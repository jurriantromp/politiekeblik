# -*- coding: utf-8 -*-

from django.core.management.base import LabelCommand
from website.models import *
import csv
import logging
from datetime import datetime
from website.management.utils import download_xml

import sys
sys.tracebacklimit=0


class Command(LabelCommand):
    help = 'Test'

    def handle(self, *args, **options):
        error_count = 0
        error_dict = {}
        henk = 0
        pt = 0

        basicFormatter = logging.Formatter("%(asctime)s %(message)s")
        #advancedFormatter = logging.Formatter("%(asctime)s pm_id: %(pm_id)s dossier: %(dossier_number)s, %(dossier_subnumber)s %(message)s")

        rootLogger = logging.getLogger()
        #childLogger = logging.getLogger('advanced')

        rootLogger.setLevel(logging.INFO)

        fileHandler = logging.FileHandler("dump/import_votes__%s.log" % datetime.now().strftime('%d%m%Y_%H%M'))
        fileHandler.setFormatter(basicFormatter)
        fileHandler.setLevel(logging.INFO)
        rootLogger.addHandler(fileHandler)

        consoleHandler = logging.StreamHandler()
        consoleHandler.setFormatter(basicFormatter)
        consoleHandler.setLevel(logging.WARN)
        rootLogger.addHandler(consoleHandler)

        logging.info("Running...")

        csv_file = args[0]

        with open(csv_file, 'rb') as csv_open:
            logging.info("Opened file %s", csv_file)
            csv_data = csv.DictReader(csv_open, delimiter=';')

            proceedings = list()
            for v in Proceeding.objects.all().values('pm_id'):  # get all existing proceedings apriori to reduce queries
                proceedings.append(v['pm_id'])

            rows = list(csv_data)
            totalrows = len(rows)
            for i, r in enumerate(rows):
                pr = dict()
                pm_id = None
                vo = None

                try:
                    pr['proceeding_type'] = r['type']
                    pr['legislative_period'] = r['legislative-period']
                    pr['session_number'] = r['session-number']
                    pr['date_of_vote'] = r['date of vote']
                    pr['title'] = r['titel document volgens stemming'][:600].decode('utf-8')
                    pr['house'] = 1 if r['#house'] == 'commons' else 0
                    pr['dossier_number'] = r['dossier nummer'][-14:] or 0 #todo validation check uitvoeren #todo een proceeding kan meerdere dossiers bevatten
                    pr['dossier_subnumber'] = r['onder nummer'] or 0 #todo 0 naar None

                    #assert (len(pr['dossier_number']) > 0), "No dossier number found"

                    pm_id = r['link stemming XML op PM'].split('/')[-1] or None
                    logging.info('Processing proceeding %s (%d/%d)', pm_id, i, totalrows)

                    #print pm_id in proceedings

                    if pm_id in proceedings:  # do a pretest to filter from previous imports
                        logging.info('Proceeding does exist, continuing..\n', extra=pr)
                        continue

                    proceeding, created = Proceeding.objects.get_or_create(pm_id=pm_id, defaults=pr)

                    logging.info('New proceeding created? %s', created)

                    if created:
                        ns = {'pmx': 'http://www.politicalmashup.nl/extra',
                          'pm': 'http://www.politicalmashup.nl',
                          'dc': 'http://purl.org/dc/elements/1.1/'}

                        xml = download_xml(settings.PM_RESOLVER + r['link stemming in context HTML op PM'].split('?')[0].split('/')[-1]) #todo tijdelijke oplossing, dit kan korter

                        categories = xml.xpath('//dc:subject/pm:categories/pm:entity', namespaces=ns)
                        if not categories:
                            logging.info('No categories found')

                        for entity in categories:
                            category = unicode(entity.xpath('string(text())', namespaces=ns).strip())
                            subcategory = unicode(entity.xpath('string(pm:entity/text())', namespaces=ns).strip())
                            logging.info('Category %s with subcategory %s found', category, subcategory)

                            pc, created = ProceedingCategory.objects.get_or_create(subcategory=subcategory, defaults={'category': category, 'subcategory': subcategory})
                            proceeding.categories.add(pc)
                            logging.info('Added subcategory to proceeding %s', subcategory)

                        passed = None
                        if r['outcome'] == 'accepted':
                            passed = True
                        elif r['outcome'] == 'rejected':
                            passed = False
                        elif r['outcome'] == 'not-available':
                            raise Exception("no outcome available: transform error?")
                        else:
                            raise Exception("no outcome available: unknown!!!")
                        logging.info('Outcome: %s', passed, extra=pr)

                        vo = Vote.objects.create(passed=passed, proceeding=proceeding)
                        vo.save()

                        try:
                            politician = None
                            if r['indiener 1, id volgens stemming']:
                                politician = Politician.objects.get(pm_id=r['indiener 1, id volgens stemming'])

                            politician2 = None
                            if r['indiener 2, id volgens stemming']:
                                politician2 = Politician.objects.get(pm_id=r['indiener 2, id volgens stemming'])

                            if politician:
                                vo.submitted_by.add(politician)
                                logging.info('Added politician %s', politician.__unicode__(), extra=pr)
                                vo.save()
                            if politician2:
                                vo.submitted_by.add(politician2)
                                logging.info('Added second politician %s', politician2.__unicode__(), extra=pr)
                                vo.save()
                        except:
                            logging.info('Policitian bestaat waarschijnlijk niet') #todo temp

                        for vot in r:
                            if vot[:4] == 'nl.p':
                                result = None
                                explicit = None

                                if r[vot] == 'aye':
                                    result = True
                                elif r[vot] == 'no':
                                    result = False
                                elif r[vot] == '':
                                    #result = False
                                    #explicit = False
                                    continue
                                else:
                                    logging.error("no vote cast available: unknown or error!!!")

                                try:
                                    party = Party.objects.get(pm_id=vot)
                                    pv = PartyVote(vote=vo, party=party, vote_result=result, explicit=explicit)
                                    pv.save()

                                    logging.info('Added vote by %s: %s %s', party, result, '(explicit)' if explicit else '', extra=pr)
                                except:
                                    logging.info('Party bestaat waarschijnlijk niet, niet toegevoegd') #todo temp

                    logging.info('Done!\n', extra=pr)

                except Exception, e:
                    error_count += 1
                    pr['pm_id'] = pm_id
                    logging.warn(e.__unicode__() + '\n', extra=pr)
                    #continue

                    for k in error_dict.keys():
                        if e[:24] in k:
                            error_dict[e.__str__()] += 1
                        else:
                            error_dict[e.__str__()] = 0

            #logging.info('Error count: %d', error_count)
            for (k, v) in error_dict.iteritems():
                logging.info('%s counted: %d', k, v, extra=pr)