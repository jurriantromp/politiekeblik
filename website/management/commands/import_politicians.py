from django.core.management.base import LabelCommand
from website.models import *
import csv
from lxml import etree
from django.db import IntegrityError
from django.core.exceptions import ObjectDoesNotExist
import urllib2
from django.conf import settings
from sorl.thumbnail import get_thumbnail
from website.management.utils import download_xml

class Command(LabelCommand):
    help = 'Test'

    def handle(self, *args, **options):
        print "Running..."

        if len(args) > 0:
            self.process_politicians(args[0])
        else:
            for house in ('commons', 'senate'):
                self.process_politicians(house)

            self.process_politicians('government')

    def process_politicians(self, house):
        created_counter = update_counter = 0
        error = list()

        ns = {'pmx': 'http://www.politicalmashup.nl/extra',
                  'pm': 'http://www.politicalmashup.nl'}

        if house == 'senate':
            hs = 0
            url = settings.PM_POLITICIANS % house
        elif house == 'commons':
            hs = 1
            url = settings.PM_POLITICIANS % house
        elif house == 'government':
            hs = 2
            url = settings.PM_GOVERNMENT
        else:
            raise Exception('Wrong argument: choose either commons, senate or government')

        print 'Downloading %s..' % url
        current = download_xml(url)

        ids = current.xpath('//member/@id', namespaces=ns)

        print 'ids in file'
        print ids
        print

        print 'database house:', hs
        print Politician.objects.filter(house=hs)
        print

        print 'Missing:'
        print Politician.objects.filter(house=hs).exclude(pm_id__in=ids)

        exit()

        for id in ids:
            xml = download_xml(settings.PM_RESOLVER + id)

            d = dict()
            d['first_name'] = unicode(xml.xpath('string(//pm:alternative-names//pm:first/text())', namespaces=ns))

            if not d['first_name']:
                d['first_name'] = unicode(xml.xpath('string(//pm:name/pm:first/text())', namespaces=ns))

            d['last_name'] = unicode(xml.xpath('string(//pm:name/pm:last/text())', namespaces=ns))

            d['house'] = hs

            photo_url = xml.xpath('string(//pm:link[@pm:linktype="photo"]/text())', namespaces=ns)
            if photo_url:
                try:
                    im = get_thumbnail(photo_url, '200x200', crop='center', quality=100) #200x200 for retina?!
                    d['image'] = im.url
                except (urllib2.HTTPError, KeyError):
                    pass

            #photo_path = None
            #if photo_url:
            #    photo_path = os.path.join("media", "politicians", os.path.basename(photo_url))
            #    #d['photo'] = photo_path
            #    #urllib.urlretrieve(photo_url, photo_path)

            #d['pm_source'] = r[1]
            d['is_active'] = True

            try:
                d['party'] = Party.objects.get(pm_id=xml.xpath('string(//pmx:party-affiliation/@pm:party-ref)', namespaces=ns))
            except ObjectDoesNotExist:
                print ">>>ERROR>>> Partij bestaat niet:", xml.xpath('string(//pmx:party-affiliation/@pm:party-ref)', namespaces=ns)
                error.append(id)
                d['party'] = None
                print d

            try:
                politician, created = Politician.objects.get_or_create(pm_id=id, defaults=d)

                if not created:
                    Politician.objects.filter(pk=politician.id).update(**d)
                    update_counter += 1
                else:
                    created_counter += 1

                print '%s:%s %s - created: %s' % (id, d['last_name'], d['party'], created)
            except IntegrityError:
                print ">>>ERROR>>> IntegrityError bij pmid:", id
                error.append(id)

            print

        print created_counter, 'created, ', update_counter, 'updated'
        print '%s were having problems' % ', '.join(error)
