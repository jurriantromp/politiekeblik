from django.contrib import admin
from website.models import *


class DefaultAdmin(admin.ModelAdmin):
    save_on_top = True


class PartyAdmin(DefaultAdmin):
    list_display = ('name', 'pm_id')
    search_fields = ('name', 'pm_id')


class CoalitionAdmin(DefaultAdmin):
    list_display = ('title', 'type')


class PoliticianAdmin(DefaultAdmin):
    list_display = ('first_name', 'last_name', 'pm_id')
    search_fields = ('first_name', 'last_name', 'pm_id')


class ProceedingAdmin(DefaultAdmin):
    list_display = ('title', 'dossier_number', 'dossier_subnumber', 'session_number', 'date_of_vote')


class VoteAdmin(DefaultAdmin):
    list_display = ('proceeding', 'passed')
    search_fields = ('proceeding__id', 'proceeding__title')


admin.site.register(Politician, PoliticianAdmin)
admin.site.register(Party, PartyAdmin)
admin.site.register(Coalition, CoalitionAdmin)
admin.site.register(Proceeding, ProceedingAdmin)
admin.site.register(Vote, VoteAdmin)
admin.site.register(PartyVote)