$(document).ready(function () {

        var page_text = '                                 Cultuur, daar geef je om       Geven en werven voor kunst en cultuur                                        Waarom         Hoe         Voordelen          Tips voor cultuurmakers                                                                                                                                 Case: Vrienden in actie                Nairac is óns museum                                                                       Speel video                                                                                                Downloads                                    Download de video als mp4 (18,0 MB)                   Download de video als mwv (4,6 MB)                   Download de ondertiteling (2,8 KB)                   Download de videotranscriptie (2,0 KB)                                                                                                                                        Case: Vrienden in actie                Nairac is óns museum                               Vrienden hebben een band met elkaar én met het museum. Het is dus veel meer dan geld geven. Goede Vrienden worden ook ambassadeur van een museum, praten enthousiast over de collectie en denken mee aan manieren om het museum te ondersteunen.                                          Sluiten                                                                                                                                                                                             Case: Vrijwilligers op Oerol                Zonder vrijwilligers geen festival                                                                       Speel video                                                                                                Downloads                                    Download de video als mp4 (21,0 MB)                   Download de video als mwv (4,4 MB)                   Download de ondertiteling (2,5 KB)                   Download de videotranscriptie (2,0 KB)                                                                                                                                        Case: Vrijwilligers op Oerol                Zonder vrijwilligers geen festival                               Het is een tijdelijke wereld. Het teamgevoel is heel groot en je werkt met z n allen heel intensief naar een bepaald moment toe.                                         Sluiten                                                                                                                                                                                             Case: Meedoen met Cinecrowd                Passie delen                                                                       Speel video                                                                                                Downloads                                    Download de video als mp4 (19,6 MB)                   Download de video als mwv (4,4 MB)                   Download de ondertiteling (3,0 KB)                   Download de videotranscriptie (2,0 KB)                                                                                                                                        Case: Meedoen met Cinecrowd                Passie delen                               Crowdfunding is denken vanuit de fans. Ze worden onderdeel van de productie en maken de realisatie van een film mogelijk.                                         Sluiten                                                                                                        Vorige slide           Volgende slide            pauzeer de carousel            Deel deze case     Facebook     Twitter                                                         Inloggen voor culturele instellingen  Nog geen inloggegevens? Stuur een e-mail naar: cultuur@daargeefjeom.nl                  Gebruikersnaam           Wachtwoord           Log in                               Waarom zou je geven om cultuur?  Verbazen, verwonderen, ontroeren, verrassen; cultuur dóet iets met je. Of je nou graag een expositie bezoekt of zelf achter de schermen werkt: cultuur beweegt je. En steeds vaker weten publiek en makers elkaar te vinden.   Cultuur. Je houdt ervan. Je geeft erom. Een wereld zonder cultuur zou een dor bestaan opleveren. En daarom deze site. Om iedereen met een hart voor cultuur bij elkaar te brengen. Want zonder bezoekers geen expositie, zonder luisteraars geen concert, zonder publiek geen voorstelling. Hoe meer mensen erom geven, hoe mooier het wordt. Geven is ook een kunst.  Cultuurgenieters ontdekken hier hoe zij hun steentje kunnen bijdragen aan hun favoriete theatergroep, museum, festival of andere culturele organisatie. Cultuurmakers krijgen hier tips over het betrekken van hun publiek, fans en vrienden om hun hart voor cultuur te verzilveren.                                     Zoveel manieren om te geven  Geven gaat over passie en enthousiasme. Geven is meer dan geld alleen. Jouw donatie is een bijdrage aan het culturele leven in Nederland. Met jouw gift kun je het fundament van culturele instellingen verstevigen. En nieuwe mogelijkheden voor instellingen, spelers en toeschouwers scheppen.   Maar wist je al dat je op veel verschillende manieren kan geven? Door bijvoorbeeld lid te worden van een  Vrienden  stichting. En wat dacht je van samen met collega s de restauratie van een kunstwerk sponsoren? Of je doneert tijd in de vorm van vrijwilligerswerk. Zo maak je alles op de eerste rang én backstage mee op bijvoorbeeld je favoriete festival.   Bekijk de Geefwijzer voor alle mogelijkheden en de interessante belastingvoordelen die cultuurdonateurs, zowel privé als bedrijfsmatig, genieten.                                             samen cultuur maken     meer over crowdfunding                                     bouwen aan de collectie     meer over Fonds op Naam                                                        tijd en inzet doneren     meer over vrijwilligerswerk                                  Al een idee aan welke culturele instelling jij wil bijdragen? Zoek een ANBI                            Wat krijg ik ervoor?  Je geeft meer dan je denkt Als je geeft aan cultuur, doneer je niet alleen geld. Je geeft het theater, museum, festival of andere culturele organisatie de kans om te bouwen aan een toekomst met nieuwe exposities of voorstellingen. Daarmee help je het rijke culturele leven in Nederland. En dat geeft niet alleen een tevreden gevoel; het kan je namelijk ook een aardige fiscale aftrekpost opleveren. Daarnaast bieden veel culturele instellingen voordelen voor gevers; zoals speciale voorstellingen, evenementen en gratis toegang.  De Geefwet maakt jouw gift meer waard Sinds de invoering van de Geefwet mag jouw gift aan een cultureel doel met 25 procent extra afgetrokken worden van de belasting. Voor bedrijven is dit zelfs 50 procent.   Ik geef 1000 euro aan het theater en door de Geefwet kost het mij slechts 350 euro. Irene van Zeeland Ontdek zelf het belastingvoordeel met de Geefwijzer De Geefwijzer wijst je de weg door de belastingregels rondom geven aan cultuur. Je zult zien: jouw steun aan dat bijzondere museum, dat vernieuwende theatergezelschap of dat swingende festival kan behalve nuttig ook nog eens fiscaal aantrekkelijk zijn. Kijk nu welke vorm van culturele donatie het beste bij jou of jouw bedrijf past.                                 De Geefwijzer  Geven aan cultuur kan fiscaal aantrekkelijk zijn. Lees hier alles over de belastingregels rond geven aan cultuur. Of je nou 10 euro stort of als bedrijf een tentoonstellingszaal wil sponsoren; groot of klein, organisatie of particulier – er is voor iedereen wel een vorm van geven die bij je past.                       Hoe wil jij aan cultuur geven?                          Geven als particulier                    Meer informatie                        Sluit informatie                                           Geven als particulier Geven kan op veel manieren en, afhankelijk van de gemaakte keuzen, de overheid faciliteert dit via de inkomstenbelasting. Een gift vindt plaats tijdens het leven. Het kan om geld gaan, maar ook om andere vermogensbestanddelen, bijvoorbeeld kunstvoorwerpen. Een gift wil zeggen dat er geen directe tegenprestatie tegenover de vrijwillige bijdrage staat. Giften kunnen onder bepaalde voorwaarden aftrekbaar zijn voor de belasting. Een ander voordeel is dat de ontvangende culturele instelling (als deze de ANBI-status heeft of een SBBI is) geen schenkbelasting hoeft te betalen, hoe groot de schenking ook is. Dit betekent dat de volledige gift ten goede komt aan kunst en cultuur.                                                               Geven als bedrijf                    Meer informatie                        Sluit informatie                            Geven als bedrijf (BVs, NVs verenigingen, stichtingen) aan cultuur                Bedrijven kunnen fiscaal voordeel hebben van het schenken aan culturele instellingen. Er moet aan een aantal voorwaarden worden voldaan. Twee soorten belasting spelen een rol: de vennootschapsbelasting en de omzetbelasting.                                                                         Is de culturele instelling een ANBI?                          Ja                                       Nee                               Wat is een ANBI                        Is de culturele instelling een ANBI?                          Ja                                       Nee                               Wat is een ANBI                      Gift (inclusief aftrek vennootschapsbelasting)            Als een bedrijf een niet-zakelijke betaling aan een ANBI verricht, kan het bedrijf mogelijk een beroep doen op de giftenaftrek. Deze aftrek bedraagt maximaal 50% van de fiscale winst per jaar met een maximum van €100.000,-. Wanneer er wel een prestatie tegenover staat, spreken we van sponsoring. Een bedrijf boekt dit dan onder de post bedrijfskosten.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Zakelijke betaling            Over het algemeen zal een bedrijf een zakelijk belang hebben bij een betaling. Dat is bijvoorbeeld het geval als sprake is van een concrete tegenprestatie, zoals bij sponsoring. Ook als er geen concreet aanwijsbare tegenprestatie is, maar het bedrijf wel een zakelijk belang heeft bij de betaling, kan mogelijk sprake zijn van een zakelijke betaling. Per geval zal moeten worden beoordeeld of het om zakelijke kosten of een gift gaat. Dit laatste is van belang omdat zakelijke betalingen als reguliere kostenpost volledig aftrekbaar zijn voor de vennootschapsbelasting, zonder drempel of maximum. De betaling heeft dus het karakter van een bedrijfslast. Als er sprake is van een directe tegenprestatie, zoals bij sponsoring, moet de culturele instelling omzetbelasting in rekening brengen, welke voor het bedrijf meestal aftrekbaar is. Als de culturele instelling als btw-ondernemer echter is vrijgesteld van btw-belastingplicht, kan onder voorwaarden gebruikgemaakt worden van een faciliteit op grond waarvan geen btw in rekening hoeft te worden gebracht. Daarbij gelden bepaalde omzetgrenzen. Ook is btw verschuldigd als vrijkaarten, speciale activiteiten of andere prestaties worden afgesproken. Vaak worden dit soort afspraken tussen een bedrijf en een culturele instelling vastgelegd in een sponsorovereenkomst. Het is uiteraard altijd mogelijk om naast de afspraken in de sponsorovereenkomst nog andere bijdragen te verstrekken.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                         Hoe wil jij geven?                          Gift                    Meer informatie                        Sluit informatie                            Geven               Geven kan op veel manieren en, afhankelijk van de gemaakte keuzen, kan dit een fiscaal voordeel opleveren. Een gift vindt plaats tijdens het leven. Het kan om geld gaan, maar ook om andere vermogensbestanddelen, bijvoorbeeld kunstvoorwerpen. Een gift wil zeggen dat er geen directe tegenprestatie tegenover de vrijwillige bijdrage staat.                                                               Nalatenschap                    Meer informatie                        Sluit informatie                            Nalaten               Naast familieleden kan in een testament ook het nalaten aan goede doelen zoals bijvoorbeeld culturele instellingen worden opgenomen. De culturele instelling kan dan tot  (mede-)erfgenaam of legataris worden benoemd. Een culturele instelling die de ANBI-status heeft, betaalt over deze verkrijging geen erfbelasting. Hierdoor komt (dit deel van) de nalatenschap volledig ten goede aan kunst en cultuur.                                                               Stichting/fonds                    Meer informatie                        Sluit informatie                            Stichting/fonds               Een van oudsher in Nederland veel voorkomende manier om cultuur te ondersteunen, is door het oprichten van een fonds of stichting. Dit kan een zelf opgerichte, onafhankelijke stichting of fonds zijn, maar een fonds kan ook worden ondergebracht bij een bestaande instelling of fonds.                                                                             Hoe wil jij geven?                          Erfbelasting met kunst betalen                                       Kunst als vrijgestelde bezitting                                               Erfbelasting met kunst betalen               De Nederlandse staat hecht er veel waarde aan dat kunst met grote betekenis voor het erfgoed behouden blijft voor de Nederlandse samenleving. Als een verzamelaar overlijdt en de nalatenschap deels bestaat uit kunstvoorwerpen, dan is het voor de erfgenamen soms moeilijk om tijdig grote geldsommen aan erfbelasting te voldoen. Daarom heeft de overheid het mogelijk gemaakt dat erfgenamen de erfbelasting kunnen voldoen met nagelaten voorwerpen met een nationaal cultuurhistorisch of kunsthistorisch belang. Dit kan kunst zijn, maar ook een archiefstuk of persoonlijke bezittingen. Het schilderij Bloeiend boompje I (1921) van Bart van der Leck is op deze wijze in museum Kröller-Müller terechtgekomen. Het moeten wel bijzondere objecten zijn: ze moeten op de lijst van beschermde voorwerpen staan die is opgesteld vanwege de Wet tot behoud van cultuurbezit of volgens de criteria van deze wet onvervangbaar en onmisbaar zijn. Als dat niet het geval is, kunnen de voorwerpen toch in aanmerking komen om als betaling te dienen als zij van groot nationaal cultuurhistorisch of kunsthistorisch belang zijn vanwege hun attractiewaarde, artistieke waarde, herkomstwaarde, ensemblewaarde of documentatiewaarde. Een speciale commissie beoordeelt of voorwerpen voor deze regeling in aanmerking komen. De erfgenamen krijgen een korting (kwijtschelding) van 120% van de waarde van de voorwerpen op de te betalen erfbelasting. Deze kwijtschelding van 120% van de waarde is bedoeld om erfgenamen te stimuleren de objecten over te dragen aan de Staat in plaats van deze te verkopen op een veiling of in het buitenland. Uiteraard bedraagt de korting niet meer dan de verschuldigde erfbelasting. De objecten die Nederland via deze regeling verwerft, worden onderdeel van de Collectie Nederland. De Staat kan de kunstwerken vervolgens in bruikleen geven aan een museum of een archief. In de praktijk is het gebruikelijk dat als een instelling het verzoek heeft ondersteund om met een bepaald object de erfbelasting te mogen betalen, de Staat het kunstwerk aan dit museum in bruikleen geeft als het goed past in de collectie van dat museum. Het is overigens raadzaam dat als u een beroep wilt doen op deze regeling, vooraf te overleggen met een museum dat interesse zou kunnen hebben in het kunstvoorwerp.                    Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Kunst als vrijgestelde bezitting              Het persoonlijk bezit van een kunstwerk zonder beleggingsdoel, bijvoorbeeld een schilderij dat thuis aan de muur hangt,  is vrijgesteld voor de inkomstenbelasting. Hierover hoeft dan ook geen 1,2% vermogensrendementsheffing in box 3 te worden betaald. Ook het (met winst of verlies) verkopen van een dergelijk kunstwerk is niet belast. Zodra sprake is van actieve handel in kunst kan het zijn dat de eigenaar van het voorwerp wordt aangemerkt als ondernemer of resultaatgenieter. In dit geval dient over de winst maximaal 52% belasting te worden betaald. Onkosten zijn in dat geval wel weer aftrekbaar. Ook kan de in rekening gebrachte btw als voorheffing in mindering worden gebracht op de over de verkoopopbrengst verschuldigde btw. Dit is een complex fiscaal terrein. Daarom wordt aangeraden om  specialistische informatie in te winnen bij een belastingadviseur. Als de kunst aan een instelling wordt uitgeleend, is deze niet meer in persoonlijk gebruik. Vermogensrendementsheffing  is dan niet meer van toepassing. Voor deze uitgeleende kunst bestaat namelijk een speciale vrijstelling.                   Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                         Wat voor gift?                          Periodieke gift                                       Andere gift                                       Kunst/andere voorwerpen                                               Periodieke gift (inclusief extra fiscaal voordeel)            Zogenoemde periodieke giften zijn volledig aftrekbaar van het inkomen voor de inkomstenbelasting. Een gift heeft nu tijdelijk extra fiscaal belastingvoordeel. De gift mag met een multiplier van 25% verhoogd worden. Dit betekent dat er een kwart meer mag worden afgetrokken. Deze extra aftrek geldt voor een maximum aan schenkingen van €5.000,- per jaar. Alle bedragen boven de €5.000,- zijn nog steeds aftrekbaar, maar hier geldt de multiplier niet. Voor de multiplier geldt een maximum van €1.250,- per jaar. Wel gelden er speciale voorwaarden. Er moet in een notariële ‘akte van schenking’ vastgelegd worden dat er ten minste vijf jaar achtereen minimaal eenmaal per jaar een vast bedrag wordt gegeven aan een ANBI-instelling (Algemeen Nut Beogende Instelling) die is aangemerkt als culturele instelling. De gift eindigt bij het overlijden van de gever. In de notariële akte kan dit ook worden vastgelegd. Informeer naar deze mogelijkheid bij de instelling van je keuze.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Andere giften             Een gewone gift kan bijvoorbeeld een jaarlijkse gift zijn die je als donateur of vriend spontaan doet of een gift ter gelegenheid van een bijzondere activiteit. Een gift heeft nu tijdelijk extra fiscaal belastingvoordeel. De gift mag met een multiplier van 25% verhoogd worden. Dit betekent dat er een kwart meer mag worden afgetrokken. De in een jaar gedoneerde ‘gewone giften’ zijn aftrekbaar voor zover deze in totaliteit meer bedragen dan €60,- of –als dat meer is– 1% van het verzamelinkomen. Tevens geldt voor de aftrekbaarheid van deze giften een maximum van 10% van dat inkomen. Gewone giften moeten wel aantoonbaar zijn voor de Belastingdienst. Dit kan bijvoorbeeld via een afschrift van de bankrekening. De aangifte inkomstenbelasting over 2012 doet men in 2013. Dus giften die je nog in 2012 doet, geef je voor 1 april 2013 op bij de belasting. Informeer naar deze mogelijkheid bij de instelling van je keuze.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Kunst of andere voorwerpen            Veel voorwerpen in musea, archieven en bibliotheken zijn geschonken. Als je een kunstwerk, archief of muziekinstrument schenkt, is dat een gift in natura. De marktwaarde van het geschonken voorwerp bepaalt dan de omvang van de gift en dus de hoogte van de aftrekpost. Bij het geven van kunst met een hoge waarde is het raadzaam om voorafgaand aan de gift de hulp van een taxateur en een notaris in te roepen en om –eventueel in samenspraak met een belastingadviseur–hierover overeenstemming te bereiken met de Belastingdienst. In de meeste gevallen wordt een groter fiscaal voordeel behaald als van deze schenking een periodieke gift wordt gemaakt. Het is verstandig om het schenken van kunst of andere voorwerpen in het testament op te nemen. Dit voorkomt onduidelijkheden voor zowel de nabestaanden als de culturele instelling.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                         Hoe wil jij nalaten?                          Testament opstellen                                       Erfstelling / legaat                                       Bruikleen / vruchtgebruik                                               Testament opstellen            specifieke wensen ten aanzien van een nalatenschap dienen te worden vastgelegd door een notaris in een testament.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Erfstelling of legaat            Bij het opstellen van een testament kan worden gekozen tussen een erfstelling of legaat voor een culturele instelling. Erfstelling is de optie waarbij de culturele instelling een erfdeel ontvangt, dus een bepaald deel of percentage van de nalatenschap. De omvang van de verkrijging hangt in dit geval af van het totaal nagelaten vermogen. Een culturele instelling zal een erfdeel van een nalatenschap alleen aanvaarden wanneer de nalatenschap positief is en hier, voor de instelling, geen negatieve voorwaarden aan verbonden zijn. In het testament wordt precies omschreven welk deel van de erfenis voor welke culturele instelling bestemd is. Een legaat houdt in dat een bepaald bedrag in geld of een goed (zoals een huis of een waardevol schilderij) wordt nagelaten aan een instelling. Over specifieke inboedelgoederen kan bij codicil worden beschikt. Notarissen kunnen je over de details informeren.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Bruikleen of vruchtgebruik            Een (deel van het) vermogen kan in bruikleen of in vruchtgebruik worden gegeven. Deze mogelijkheid is er ook tijdens het leven. In het testament kan bijvoorbeeld worden vastgelegd dat een (deel van het) vermogen eigendom wordt van de erfgenamen, maar dat het gebruik of het rendement uit dit vermogen gedurende een vastgestelde periode ten goede komt aan een culturele instelling.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                         Wat voor stichting of fonds?                          Zelf oprichten                                       Fonds op naam                                               Zelf opgerichte stichting            Een (deel van het) vermogen kan worden ondergebracht in een onafhankelijke zelf op te richten stichting. De stichting moet worden opgericht bij notariële akte. Wanneer de stichting door de Belastingdienst als algemeen nut beogende instelling (ANBI) is geregistreerd, is deze transactie vrij van schenkbelasting. De doelstelling van de stichting kan door de oprichter zelf worden bepaald. Bij de  notaris of belastingadviseur is meer informatie te vinden over  de stichtingsakte, statuten, reglementen en dergelijke.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                               Fonds op naam bij instelling            Steeds meer culturele instellingen (met ANBI-status) en grote fondsen bieden de mogelijkheid een Fonds op Naam op te richten dat wordt ondergebracht in de instelling zelf of bij een steunstichting. Vaak wordt hieraan een minimumbedrag gekoppeld, bijvoorbeeld € 50.000,-. De oprichter mag de naam en de bestemming van het fonds bepalen. Natuurlijk moet de bestemming wel passen binnen de doelstelling van de instelling waar het fonds is ondergebracht. Het fonds of de culturele instelling zorgt ervoor dat het geld besteed wordt aan het specifieke doel en draagt zorg voor de administratieve afhandeling. Zo profiteert de oprichter van een Fonds op Naam van de kennis en ervaring van de culturele instelling, zonder zorgen over organisatorische en administratieve zaken. Een Fonds op Naam kan zowel bij leven als bij testament opgericht worden. Voor de oprichting van een Fonds op Naam is een notariële akte verplicht. Het oprichten van een Fonds op Naam, waarbij giften aan een culturele instelling worden gedaan in de vorm van periodieke giften, is fiscaal het aantrekkelijkst omdat je dan volledige aftrek voor de inkomstenbelasting geniet inclusief het gebruik van de multiplier (zie onder periodieke gift in de ‘Geefwijzer’). De instelling moet voor het fonds de ANBI-status hebben.                 Bekijk het overzicht met culturele instellingen       Download de brochure als pdf (424,5 KB)       Mail deze informatie                                             Heb je een vraag?       cultuur@daargeefjeom.nl                 Volg ons       Facebook       Twitter       Youtube                                                                                                         Meer informatie              Copyright              Cookies              Sitemap                        Deze website maakt gebruik van cookies. Door verder te gaan stemt u hiermee in. Lees meer over cookies                                                                                                                                 ';
//alert(page_text);
        var diameter = 800 - 30,
            limit = 5000,
            format = d3.format(",d"),
            color = d3.scale.category20c();

        var bubble = d3.layout.pack()
            .sort(null)
            .size([diameter, diameter])
            .padding(1.5);

        var svg = d3.select("#svgid").append("svg")
            .attr("width", diameter)
            .attr("height", diameter)
            .attr("class", "bubble");


        var data = [
            ["Tea", "Coffee", "Soda", "Chips", "Milk", "Chocolate", "Beer", "Wine", "Tobacco", "gold", "silver"],
            [130, 30, 200, 40, 230, 150, 80, 65, 20, 10, 100]
        ];

        var stopwords = {"com": 1, "amp": 1, "http": 1, "href": 1, "statuses": 1, "search": 1, "ago": 1, "link": 1, "hours": 1, "comments": 1, "a": 1, "about": 1, "above": 1, "above": 1, "across": 1, "after": 1, "afterwards": 1, "again": 1, "against": 1, "all": 1, "almost": 1, "alone": 1, "along": 1, "already": 1, "also": 1, "although": 1, "always": 1, "am": 1, "among": 1, "amongst": 1, "amoungst": 1, "amount": 1, "an": 1, "and": 1, "another": 1, "any": 1, "anyhow": 1, "anyone": 1, "anything": 1, "anyway": 1, "anywhere": 1, "are": 1, "around": 1, "as": 1, "at": 1, "back": 1, "be": 1, "became": 1, "because": 1, "become": 1, "becomes": 1, "becoming": 1, "been": 1, "before": 1, "beforehand": 1, "behind": 1, "being": 1, "below": 1, "beside": 1, "besides": 1, "between": 1, "beyond": 1, "bill": 1, "both": 1, "bottom": 1, "but": 1, "by": 1, "call": 1, "can": 1, "cannot": 1, "cant": 1, "co": 1, "con": 1, "could": 1, "couldnt": 1, "cry": 1, "de": 1, "describe": 1, "detail": 1, "do": 1, "done": 1, "down": 1, "due": 1, "during": 1, "each": 1, "eg": 1, "eight": 1, "either": 1, "eleven": 1, "else": 1, "elsewhere": 1, "empty": 1, "enough": 1, "etc": 1, "even": 1, "ever": 1, "every": 1, "everyone": 1, "everything": 1, "everywhere": 1, "except": 1, "few": 1, "fifteen": 1, "fify": 1, "fill": 1, "find": 1, "fire": 1, "first": 1, "five": 1, "for": 1, "former": 1, "formerly": 1, "forty": 1, "found": 1, "four": 1, "from": 1, "front": 1, "full": 1, "further": 1, "get": 1, "give": 1, "go": 1, "had": 1, "has": 1, "hasnt": 1, "have": 1, "he": 1, "hence": 1, "her": 1, "here": 1, "hereafter": 1, "hereby": 1, "herein": 1, "hereupon": 1, "hers": 1, "herself": 1, "him": 1, "himself": 1, "his": 1, "how": 1, "however": 1, "hundred": 1, "ie": 1, "if": 1, "in": 1, "inc": 1, "indeed": 1, "interest": 1, "into": 1, "is": 1, "it": 1, "its": 1, "itself": 1, "keep": 1, "last": 1, "latter": 1, "latterly": 1, "least": 1, "less": 1, "ltd": 1, "made": 1, "many": 1, "may": 1, "me": 1, "meanwhile": 1, "might": 1, "mill": 1, "mine": 1, "more": 1, "moreover": 1, "most": 1, "mostly": 1, "move": 1, "much": 1, "must": 1, "my": 1, "myself": 1, "name": 1, "namely": 1, "neither": 1, "never": 1, "nevertheless": 1, "next": 1, "nine": 1, "no": 1, "nobody": 1, "none": 1, "noone": 1, "nor": 1, "not": 1, "nothing": 1, "now": 1, "nowhere": 1, "of": 1, "off": 1, "often": 1, "on": 1, "once": 1, "one": 1, "only": 1, "onto": 1, "or": 1, "other": 1, "others": 1, "otherwise": 1, "our": 1, "ours": 1, "ourselves": 1, "out": 1, "over": 1, "own": 1, "part": 1, "per": 1, "perhaps": 1, "please": 1, "put": 1, "rather": 1, "re": 1, "same": 1, "see": 1, "seem": 1, "seemed": 1, "seeming": 1, "seems": 1, "serious": 1, "several": 1, "she": 1, "should": 1, "show": 1, "side": 1, "since": 1, "sincere": 1, "six": 1, "sixty": 1, "so": 1, "some": 1, "somehow": 1, "someone": 1, "something": 1, "sometime": 1, "sometimes": 1, "somewhere": 1, "still": 1, "such": 1, "system": 1, "take": 1, "ten": 1, "than": 1, "that": 1, "the": 1, "their": 1, "them": 1, "themselves": 1, "then": 1, "thence": 1, "there": 1, "thereafter": 1, "thereby": 1, "therefore": 1, "therein": 1, "thereupon": 1, "these": 1, "they": 1, "thickv": 1, "thin": 1, "third": 1, "this": 1, "those": 1, "though": 1, "three": 1, "through": 1, "throughout": 1, "thru": 1, "thus": 1, "to": 1, "together": 1, "too": 1, "top": 1, "toward": 1, "towards": 1, "twelve": 1, "twenty": 1, "two": 1, "un": 1, "under": 1, "until": 1, "up": 1, "upon": 1, "us": 1, "very": 1, "via": 1, "was": 1, "we": 1, "well": 1, "were": 1, "what": 1, "whatever": 1, "when": 1, "whence": 1, "whenever": 1, "where": 1, "whereafter": 1, "whereas": 1, "whereby": 1, "wherein": 1, "whereupon": 1, "wherever": 1, "whether": 1, "which": 1, "while": 1, "whither": 1, "who": 1, "whoever": 1, "whole": 1, "whom": 1, "whose": 1, "why": 1, "will": 1, "with": 1, "within": 1, "without": 1, "would": 1, "yet": 1, "you": 1, "your": 1, "yours": 1, "yourself": 1, "yourselves": 1, "the": 1};

        var title = [];
        var users = [];
        var comments = [];
        var score = [];
        var wordList = []; //each word one entry and contains the total count [ {cnt:30,title_list:[3,5,9],
        var wordCount = [];
        var wordMap = {};
        var wordIdList = [];
        var wordTitleMap = [];
        var minVal = 10000;
        var maxVal = -230;
        var regex = /\s|\.|,|;|[^a-zA-Z0-9]/g;
        var words;
        var wordId = 0;
        var wordStr = "";
        var titleID = 0;
        var tokens = page_text.split(regex);
        var totalWords = tokens.length;
        for (var i = 0; i < tokens.length; i++) {
            wordStr = tokens[i];
            try {

                {

                    if (typeof(wordStr) != "undefined" && wordStr.length > 2) {
                        wordStr = wordStr.toLowerCase();
                        if (stopwords[wordStr] == 1) {
                            continue; //skip the stop words;
                        }

                        if (typeof(wordMap[wordStr]) == "undefined") {

                            wordList.push(wordStr);
                            wordCount.push(1);
                            wordMap[wordStr] = wordId;
                            wordIdList.push(wordId);
                            wordId++;

                        }
                        else {
                            wordCount[wordMap[wordStr]]++;
                        }
                    }
                }
            }
            catch (err) {

            }


        }

        wordIdList.sort(function (x, y) {
                return -wordCount[x] + wordCount[y]
            }
        );

        var wordPercentStr = "<p>Total words on the page=" + totalWords + " , Words used in Visualization=" + wordId + "</p>";
        wordPercentStr += "<table><tr><td>Word</td><td>Occurence/count</td><td>Good Density (%)</td><td>Gross Density (%)</td></tr>";
        var wi = 0;
        var density;
        var grossDensity;
        for (var wp = 0; wp < wordIdList.length; wp++) {
            wi = wordIdList[wp];
            density = " " + (wordCount[wi] * 100 / wordId);
            density = density.substr(0, 6);
            grossDensity = (" " + (wordCount[wi] * 100 / totalWords)).substr(0, 6);
            wordPercentStr += "<tr>";
            wordPercentStr += "<td>" + wordList[wi] + "</td><td>" + wordCount[wi] + "</td><td>" + density + "</td><td>" + grossDensity + "</td>";
            wordPercentStr += "</tr>";
        }
        wordPercentStr += "</table>";
        $("#topwords").html(wordPercentStr);
        $("#countbox").text(wordId);

        minVal = 10000;
        maxVal = -100;
        for (var wi = 0; wi < wordList.length; wi++) {
            if (minVal > wordCount[wi]) minVal = wordCount[wi];
            if (maxVal < wordCount[wi]) maxVal = wordCount[wi];

        }
        var data = [
//["Tea","Coffee","Soda","Chips","Milk","Chocolate","Beer","Wine","Tobacco","gold","silver"],
//[130,30,200,40,230,150,80,65,20,10,100]
//users,
//score
            wordList,
            wordCount
        ];

        var dobj = [];

        for (var di = 0; di < data[0].length; di++) {
            dobj.push({"key": di, "value": data[1][di]});


        }
        display_pack({children: dobj});
//d3.json("flare.json", function(error, root)
        function display_pack(root) {
            var node = svg.selectAll(".node")
                    .data(bubble.nodes(root)
                        .filter(function (d) {
                            return !d.children;
                        }))
                    .enter().append("g")
                    .attr("class", "node")
                    .attr("transform", function (d) {
                        return "translate(" + d.x + "," + d.y + ")";
                    })
                    .style("fill", function (d) {
                        return color(data[0][d.key]);
                    })
                    .on("mouseover", function (d, i) {
                        d3.select(this).style("fill", "gold");
                        showToolTip(" " + data[0][i] + "<br>" + data[1][i] + " ", d.x + d3.mouse(this)[0] + 50, d.y + d3.mouse(this)[1], true);
                        //console.log(d3.mouse(this));
                    })
                    .on("mousemove", function (d, i) {

                        tooltipDivID.css({top: d.y + d3.mouse(this)[1], left: d.x + d3.mouse(this)[0] + 50});
                        //showToolTip("<ul><li>"+data[0][i]+"<li>"+data[1][i]+"</ul>",d.x+d3.mouse(this)[0]+10,d.y+d3.mouse(this)[1]-10,true);
                        //console.log(d3.mouse(this));
                    })
                    .on("mouseout", function () {
                        d3.select(this).style("fill", function (d) {
                            return color(data[0][d.key]);
                        });
                        showToolTip(" ", 0, 0, false);
                    })
                ;

            /*node.append("title")
             .text(function(d) { return data[0][d.key] + ": " + format(d.value); });
             */
            node.append("circle")
                .attr("r", function (d) {
                    return d.r;
                })
            ;
            //.style("fill", function(d) { return color(data[0][d.key]); });

            node.append("text")
                .attr("dy", ".3em")
                .style("text-anchor", "middle")
                .style("fill", "black")
                .text(function (d) {
                    return data[0][d.key].substring(0, d.r / 3);
                });
        }

//);


        function showToolTip(pMessage, pX, pY, pShow) {
            if (typeof(tooltipDivID) == "undefined") {
                tooltipDivID = $('<div id="messageToolTipDiv" style="position:absolute;display:block;z-index:10000;border:2px solid black;background-color:rgba(0,0,0,0.8);margin:auto;padding:3px 5px 3px 5px;color:white;font-size:12px;font-family:arial;border-radius: 5px;vertical-align: middle;text-align: center;min-width:50px;overflow:auto;"></div>');

                $('body').append(tooltipDivID);
            }
            if (!pShow) {
                tooltipDivID.hide();
                return;
            }
            //MT.tooltipDivID.empty().append(pMessage);
            tooltipDivID.html(pMessage);
            tooltipDivID.css({top: pY, left: pX});
            tooltipDivID.show();
        }

//d3.select(self.frameElement).style("height", diameter + "px");

    }
) //document ready