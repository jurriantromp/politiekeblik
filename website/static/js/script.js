function Application() {
}
Application.prototype = {
    //politician: undefined,

    init: function () {
        //this.setupD3Bubbles(); //priority
        //this.setupViewportHeight();
        //this.setupJsonHandler();
        //this.setupEvents();
        //this.setupPolitician();
        this.setupAmber();

        //this.setupHighchartSeniority();
    },

    setupChosen: function() {
        $("select").chosen({disable_search_threshold: 10});
    },


    setupAmber: function() {

        window.App = Ember.Application.create();

        App.Router.map(function() {
          this.resource('politician', { path: '/nl/m/:pmid' });
          this.resource('party', {path: '/nl/p/:pmid'});
          this.resource('not-found', {path: '/not-found'});
        });

        //App.Router.reopen({
        //  location: 'history'
        //});

        $that = this;
        Ember.View.reopen({
            didInsertElement : function() {
                this._super();
                Ember.run.scheduleOnce('afterRender', this, this.didRenderElement);
            },
            didRenderElement : function() {
                $that.setupChosen();
            }
        });

        Ember.Route.reopen({
          activate: function() {
            var cssClass = this.toCssClass();
            // you probably don't need the application class
            // to be added to the body
            if (cssClass != 'application') {
              Ember.$('body').addClass(cssClass);
            }
          },
          deactivate: function() {
            Ember.$('body').removeClass(this.toCssClass());
          },
          toCssClass: function() {
            return this.routeName.replace(/\./g, '-').dasherize();
          }
        });

        App.ApplicationController = Ember.ObjectController.extend({
            actions: {
                changePolitician: function() {
                    var politician = $('#select-politician option:selected').text().split(".");
                    var id = politician.pop();
                    var type = politician.pop();

                    if (type == 'm'){
                        this.transitionToRoute('politician', id);
                    }
                    else if (type == 'p'){
                        this.transitionToRoute('party', id);
                    }
                }
            }
        });


//        App.LoadingRoute = Ember.Route.extend({
//          renderTemplate: function() {
//            this.render('loading');
//          }
//        });

        App.PoliticianController = Ember.ObjectController.extend({

            partySupportOptions: function() {
                return {
                    chart: {
                        renderTo: this.get('id'),
                        type: 'bar'
                    },

//                    title: {
//                        text: '<h3>Steun van andere partijen</h3>',
//                        useHTML: true
//                    },

                    xAxis: [
                        {
                            categories: this.get('model.data')[0],
                            reversed: true,
                            labels: {
                                enabled: false
                            },
                            lineWidth: 0,
                            tickLength: 0
                        },
                        { // mirror axis on right side
                            opposite: true,
                            reversed: true,
                            categories: this.get('model.data')[0],
                            linkedTo: 0,
                            labels: {
                                enabled: false
                            },
                            lineWidth: 0,
                            tickLength: 0
                        }
                    ],

                    yAxis: {
                        allowDecimals: true,
                        reversed: true,
                        //min: -3,
                        //max: 3,
                        endOnTick: false,
                        maxPadding: 0.1,
                        minPadding: 0.1,
                        gridLineWidth: 0,
                        stackLabels: {
                            style: {
                                color: 'white'
                            },
                            enabled: true
                        },
                        labels: {
                            enabled: false
                        },
                        title: {
                            text: null
                        }
                    },

                    tooltip: {
                        formatter:function() {
                            return '<span>'+ this.x +'</span>: <b>'+ Highcharts.numberFormat(this.points[0].y / (this.points[0].y - this.points[1].y) * 100, 1, '.') + '%' +'</b> voorstellen gesteund<br/>';
                        },


                        shared: true
                    },

                    plotOptions: {
                        bar: {
                            stacking: 'normal',
                        }
                    },

                    series: [
                        {
                            name: 'Voor', //this.politician.name, // A voor
                            //color: '#DB5656',
                            data: this.get('model.data')[1],

                        },
                        {
                            name: 'Tegen', //this.politician.name, // A tegen
                            //color: '#B15E5E',
                            data: this.get('model.data')[2],
                            //linkedTo: ':previous'
                        }
                    ],

                    legend: {
                        enabled: false
                    }

                }
            }.property('model.partySupportOptions'),

            totalSupportOptions: function() {
                return {
                    chart: {
                        renderTo: this.get('id'),
                        type: 'pie',
                        margin: [0, 0, 0, 0]
                    },
                    series: [
                        {
                            name: 'Percentage',
                            data: [
                                {
                                     name: "Aangenomen",
                                    y: this.get('model.statistics')[3]
                                },
                                {
                                    name: "Verworpen",
                                    y: 100 - this.get('model.statistics')[3]
                                }
                            ],
                            size: '100%',
                            innerSize: '95%',
                            dataLabels: {
                                enabled: false
                            }

                        }
                    ]
                }
            }.property('model.totalSupportOptions'),

            firstCategoryOptions: function() {
                return {
                    chart: {
                        renderTo: this.get('id'),
                        type: 'pie',
                        margin: [0, 0, 0, 0]
                    },
                    series: [
                        {
                            name: 'Percentage',
                            data: [
                                {
                                     name: "Aangenomen",
                                    y: this.get('model.categories')[0]['percent']
                                },
                                {
                                    name: "Verworpen",
                                    y: 100 - this.get('model.categories')[0]['percent']
                                }
                            ],
                            size: '80%',
                            innerSize: '65%',
                            dataLabels: {
                                enabled: false
                            }
                        },
                        {
                            name: 'Aantal stemmingen',
                            data: [
                                {
                                     name: "Moties",
                                    y: this.get('model.categories')[0]['motions']
                                },
                                {
                                    name: "Amendementen",
                                    y: this.get('model.categories')[0]['amendments']
                                }
                            ],
                            size: '100%',
                            innerSize: '98%',
                            endAngle: 360 * this.get('model.categories')[0]['total'] / this.get('model.statistics')[0],
                            dataLabels: {
                                enabled: false
                            }
                        }
                    ]
                }
            }.property('model.firstCategoryOptions'),

            secondCategoryOptions: function() {
                return {
                    chart: {
                        renderTo: this.get('id'),
                        type: 'pie',
                        margin: [0, 0, 0, 0]
                    },
                    series: [
                        {
                            name: 'Percentage',
                            data: [
                                {
                                     name: "Aangenomen",
                                    y: this.get('model.categories')[1]['percent']
                                },
                                {
                                    name: "Verworpen",
                                    y: 100 - this.get('model.categories')[1]['percent']
                                }
                            ],
                            size: '80%',
                            innerSize: '65%',
                            dataLabels: {
                                enabled: false
                            }
                        },
                        {
                            name: 'Aantal stemmingen',
                            data: [
                                {
                                     name: "Moties",
                                    y: this.get('model.categories')[1]['motions']
                                },
                                {
                                    name: "Amendementen",
                                    y: this.get('model.categories')[1]['amendments']
                                }
                            ],
                            size: '100%',
                            innerSize: '98%',
                            endAngle: 360 * this.get('model.categories')[1]['total'] / this.get('model.statistics')[0],
                            dataLabels: {
                                enabled: false
                            }
                        }
                    ]
                }
            }.property('model.secondCategoryOptions')
    });

        App.PartyController = App.PoliticianController.extend({
        });

        App.PoliticianRoute = Ember.Route.extend({
          actions: {
            error: function() {
              this.transitionTo('not-found');
            }
          },

          model: function(params) {
            var pmid = params.pmid.split(".").pop();
            return $.getJSON('/json/politician/nl.m.'+ pmid +'/').then(function(data){
               return data;
            });
          }
        });

        App.PartyRoute = Ember.Route.extend({
          model: function(params) {
            var pmid = params.pmid.split(".").pop();
            return $.getJSON('/json/party/nl.p.'+ pmid +'/').then(function(data){
               return data;
            });
          },
          renderTemplate: function(controller){
            this.render('politician', {controller: controller});
          }
        });

//        App.NotFoundRoute = Ember.Route.extend({
//          renderTemplate: function() {
//            this.render('not-found');
//          }
//        });

        App.HighchartsThemeMixin = Ember.Mixin.create({
           buildTheme: function() {
              Highcharts.theme = {
                colors: ['#3471BA', '#E9312A'],
                chart: {
                    backgroundColor: null
                },
                title: {
                    text: null,
                    style: {
                        color: '#000',
                        font: 'bold 16px "Trebuchet MS", Verdana, sans-serif'
                    }
                },
                subtitle: {
                    text: null,
                    style: {
                        color: '#666666',
                        font: 'bold 12px "Trebuchet MS", Verdana, sans-serif'
                    }
                },
                credits: false,

                plotOptions: {
                    pie: {
                        //shadow: true,
                        center: ['50%', '50%'],
                        borderWidth: 0
                        //size: '90%'
                    }
                },

                legend: {
                    enable: false,
                    itemStyle: {
                        font: '9pt Trebuchet MS, Verdana, sans-serif',
                        color: 'black'
                    },
                    itemHoverStyle:{
                        color: 'gray'
                    }
                }

              };
            return Highcharts.setOptions(Highcharts.theme);
           }
        });

        App.HighChartsComponent = Ember.Component.extend(App.HighchartsThemeMixin, {
          classNames:   [ 'highcharts-wrapper' ],
          content:      undefined,
          chartOptions: undefined,
          chart:        null,

          buildOptions: Em.computed('chartOptions', 'content.@each.isLoaded', function() {
            var chartContent, chartOptions, defaults;
            chartOptions = this.getWithDefault('chartOptions', {});
            chartContent = this.get('content.length') ? this.get('content') : [
              {
                id: 'noData',
                data: 0,
                color: '#aaaaaa'
              }
            ];
            defaults = {
              series: chartContent
            };

            return Em.merge(defaults, chartOptions);
          }),

          _renderChart: (function() {
            this.drawLater();
            this.buildTheme();
          }).on('didInsertElement'),

          contentDidChange: Em.observer('model.statistics', function() {
            alert();
            var chart;
            if (!(this.get('content') && this.get('chart'))) {
              return;
            }
            chart = this.get('chart');
            return this.get('content').forEach(function(series, idx) {
              var _ref;
              if ((_ref = chart.get('noData')) != null) {
                _ref.remove();
              }
              if (chart.series[idx]) {
                return chart.series[idx].setData(series.data);
              } else {
                return chart.addSeries(series);
              }
            });
          }),

          drawLater: function() {
            Ember.run.scheduleOnce('afterRender', this, 'draw');
          },

          draw: function() {
            var options;
            options = this.get('buildOptions');
            this.set('chart', this.$().highcharts(options).highcharts());
            //console.log(this.chart);


//            patternA = this.createElement('pattern').add();
////            .attr({
////                id: 'photo',
////                patternUnits: 'userSpaceOnUse',
////                x: '0',
////                y: '0',
////                width: '1',
////                height: '1'
////            }).add();
//
//            //console.log(patternA, this.chart.renderer.defs);
//            //this.chart.renderer.defs.add(patternA);
//            //console.log(this.chart.renderer.defs);
//
//            this.chart.renderer.image('http://localhost:8000/media/cache/4e/cb/4ecb9c7e195d03b9b073f70cd077eda8.jpg', 20, 20, 150, 100).attr({
//                id: 'photo',
//                patternUnits: 'userSpaceOnUse'
//            }).add(patternA);
//
//            this.chart.renderer.circle(this.get('model.photo'), 150, 100).attr({
//                fill: 'url(#photo)',
//                stroke: 'black',
//                'stroke-width': 1
//            }).add();

            //              <svg width="700" height="660">
            //  <defs>
            //    <pattern id="image" x="0" y="0" patternUnits="userSpaceOnUse" height="1" width="1">
            //      <image x="0" y="0" xlink:href="url.png"></image>
            //    </pattern>
            //  </defs>
            //  <circle id='top' cx="180" cy="120" r="80" fill="url(#image)"/>
            //</svg>
          },

          _destroyChart: (function() {
            this._super();
            this.get('chart').destroy();
          }).on('willDestroyElement')
        });
    }
};

var app = new Application();
$(document).ready(function () {
    app.init();
}); // end document.ready
