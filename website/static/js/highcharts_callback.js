function (chart) {
        var r = chart.renderer;
        var pattern = r.createElement("pattern")
            .attr({
                id: "photo",
                patternUnits: "userSpaceOnUse",
                x: 22,
                y: 22,
                width: 154,
                height: 154,
            })
            .add(r.defs);

        r.image("%(url)s", 0, 0, 154, 154)
        .add(pattern);

        r.circle(100, 100, 77, 77).attr({
            fill: "url(#photo)"
        }).add();
}
