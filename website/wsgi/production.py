import os, site, sys

os.environ.setdefault("DJANGO_SETTINGS_MODULE", "website.settings")

sys.path.append('/var/www/politiekinzicht/production/')
sys.path.append('/var/www/politiekinzicht/production-env/lib/python2.7/site-packages/')

# This application object is used by the development server
# # as well as any WSGI server configured to use this file.
from django.core.wsgi import get_wsgi_application
application = get_wsgi_application()

# New relic settings
try:
    import newrelic.agent
    #sys.path.append('/usr/lib64/python2.6/site-packages/newrelic-2.4.0.4/')
    os.environ.setdefault("NEW_RELIC_CONFIG_FILE", "newrelic.ini newrelic-admin run-program command options")
    newrelic.agent.initialize('/var/www/politiekinzicht/production/newrelic.ini')
    application = newrelic.agent.wsgi_application()(application)
except ImportError:
    pass