from django.db import models
from django.conf import settings
from .storage import OverwriteStorage
#from website.decorators import method_cache
#import operator
#import traceback, sys


class ActiveManager(models.Manager):
    def get_query_set(self):
        return super(ActiveManager, self).get_query_set().filter(is_active=True)

        #def update(self, *args, **kwargs): #todo: make this work
        #    # if update field is '' or None then skip
        #    print "UPDATE"
        #    return super(ActiveManager, self).get_query_set().update(*args, **kwargs)


class PoliticalMashup(models.Model):
    pm_id = models.CharField(max_length=50, db_index=True, unique=True, help_text="PoliticalMashup id")
    #pm_source = models.URLField(max_length=100, blank=True, null=True) #todo: overruled

    @property
    def pm_source(self):
        return settings.PM_RESOLVER + self.pm_id

    class Meta:
        abstract = True


class Politician(PoliticalMashup):
    first_name = models.CharField(max_length=40)
    last_name = models.CharField(max_length=40)
    party = models.ForeignKey('Party', blank=True, null=True)
    image = models.CharField(blank=True, max_length=100)
    rendered_circle = models.ImageField(upload_to='rendered_circles', storage=OverwriteStorage(), blank=True)
    house = models.SmallIntegerField(choices=((0, 'Eerste kamer'), (1, 'Tweede kamer'), (2, 'Regering')))
    seniority = models.DateField(blank=True, null=True)
    twitter = models.CharField(max_length=15, blank=True, help_text="Zonder @")
    is_active = models.BooleanField(default=True)
    #active_since = models.DateField(blank=True, null=True)
    #inactive_since = models.DateField(blank=True, null=True)

    active = ActiveManager()
    objects = active #models.Manager()

    #@property
    #def is_active:
    #    return between active_since and active_end

    @property
    def name(self):
        return '%s %s' % (self.first_name, self.last_name)

    #@method_cache(86400)
    def submit_statistics(self, first_date=None, last_date=None):
        params = [self.id]
        query = """
            SELECT vs.id, COUNT(wv.id) AS total, SUM(IF(passed = 1,1,0)) AS passed, SUM(IF(passed = 0,1,0)) AS failed, SUM(IF(passed = 1,1,0)) / COUNT(passed) * 100 AS percent, SUM(IF(proceeding_type = 'wetsvoorstel',1,0)) AS bills, SUM(IF(proceeding_type = 'motie',1,0)) AS motions, SUM(IF(proceeding_type = 'amendement',1,0)) AS amendments
            FROM (
                SELECT vote_id, politician_id as id
                FROM website_vote_submitted_by
                WHERE politician_id = %s
            ) AS vs
            JOIN website_vote AS wv ON wv.id = vs.vote_id
            JOIN website_proceeding AS wr ON wr.id = wv.proceeding_id
            """
        if first_date and last_date:
            query += """
            WHERE wr.date_of_vote BETWEEN '%s' AND '%s'"""
            params.extend([first_date, last_date])

        for politician in Politician.objects.raw(query, params):  # todo exception?
            if politician.total > 0:
                return [int(politician.total), int(politician.passed), int(politician.failed),
                        round(politician.percent, 1), int(politician.motions), int(politician.amendments),
                        int(politician.bills)]
            return [0, 0, 0, 0.0, 0, 0, 0]  # todo geen data gevonden template ipv van 0 tonen

    #@method_cache(86400)
    def submit_support(self, first_date=None, last_date=None):
        params = [self.id]
        query = """
            SELECT *, abs_aye / total * 100 AS rel_aye, abs_no / total * 100 AS rel_no
            FROM (
                SELECT pa.id, pa.abbreviation, COUNT(vote_result) AS total, SUM(IF(vote_result = 1,1,0)) AS abs_aye, SUM(IF(vote_result = 0,1,0)) AS abs_no
                FROM (
                    SELECT vote_id, party_id
                    FROM website_vote_submitted_by AS wv
                    LEFT JOIN website_politician AS wp ON wv.politician_id = wp.id
                    WHERE wp.id = %s
                ) AS vs
                JOIN website_vote AS wv ON wv.id = vs.vote_id
                JOIN website_partyvote AS pv ON pv.vote_id = wv.id AND vs.party_id != pv.party_id
                JOIN website_party AS pa ON pv.party_id = pa.id"""
        if first_date and last_date:
            query += """
                JOIN website_proceeding AS wr ON wr.id = wv.proceeding_id
                WHERE wr.date_of_vote BETWEEN '%s' AND '%s'"""
            params.extend([first_date, last_date])
        query += """
                GROUP BY pa.id
            ) AS a
            WHERE abs_aye != 0 OR abs_no != 0
            ORDER BY abs_aye DESC, total ASC, RAND()"""

        results = []
        for party in Party.objects.raw(query, params):  # todo exception?
            #print round(party.rel_aye, 1), type(round(party.rel_aye, 1))
            results.append(
                [party.abbreviation, int(party.total), int(party.abs_aye), int(party.abs_no), round(party.rel_aye, 1),
                 round(party.rel_no, 1)])
        return results

    #@method_cache(86400)
    def category_support(self, first_date=None, last_date=None, limit=3):
        params = [self.id, self.id]
        query = """
            SELECT prc.id, a.category, a.subcategory, SUM(IF(passed = 1,1,0)) AS passed, SUM(IF(passed = 0,1,0)) AS failed, SUM(IF(passed = 1,1,0)) / COUNT(passed) * 100 AS percent, total, SUM(IF(proceeding_type = 'wetsvoorstel',1,0)) AS bills, SUM(IF(proceeding_type = 'motie',1,0)) AS motions, SUM(IF(proceeding_type = 'amendement',1,0)) AS amendments
            FROM (
                SELECT vote_id, ws.politician_id AS id
                FROM website_vote_submitted_by AS ws
                WHERE ws.politician_id = %s
            ) AS vs
            JOIN website_vote AS wv ON wv.id = vs.vote_id
            JOIN website_proceeding AS pr ON pr.id = wv.proceeding_id
            JOIN website_proceeding_categories AS pc ON pr.id = pc.proceeding_id
            JOIN (
                SELECT pc.proceedingcategory_id, category, subcategory, COUNT(wv.id) AS total
                FROM (
                    SELECT vote_id
                    FROM website_vote_submitted_by
                    WHERE politician_id = %s
                ) AS vs
                JOIN website_vote AS wv ON wv.id = vs.vote_id
                JOIN website_proceeding AS wr ON wr.id = wv.proceeding_id
                JOIN website_proceeding_categories AS pc ON wr.id = pc.proceeding_id
                JOIN website_proceedingcategory AS prc ON prc.id = pc.proceedingcategory_id
                GROUP BY pc.proceedingcategory_id
                ORDER BY total DESC
                LIMIT 2
            ) AS a ON pc.proceedingcategory_id = a.proceedingcategory_id
            JOIN website_proceedingcategory AS prc ON a.proceedingcategory_id = prc.id
            """
        if first_date and last_date:
            query += """
            WHERE pr.date_of_vote BETWEEN '%s' AND '%s'"""
            params.extend([first_date, last_date])
        query += """
            GROUP BY prc.id
            ORDER BY total DESC"""

        results = []
        #todo limit
        for category in ProceedingCategory.objects.raw(query, params):  # todo exception?
            #print round(party.rel_aye, 1), type(round(party.rel_aye, 1))
            results.append(
                {'category': category.category, 'subcategory': category.subcategory, 'passed': int(category.passed),
                 'failed': int(category.failed), 'percent': round(category.percent, 1), 'total': int(category.total),
                 'motions': int(category.motions), 'amendments': int(category.amendments), 'bills': int(category.bills)})
        return results

    def __unicode__(self):
        return self.name

    class Meta:
        ordering = ['last_name']


class Party(PoliticalMashup):
    name = models.CharField(max_length=100)
    abbreviation = models.CharField(max_length=40, blank=True)
    #todo abbr_preferred = models.BooleanField(default=False)
    seats_commons = models.PositiveSmallIntegerField(blank=True) #todo: make this a calculated politician thingy
    seats_senate = models.PositiveSmallIntegerField(blank=True)
    image = models.CharField(blank=True, max_length=100)
    rendered_circle = models.ImageField(upload_to='rendered_circles', storage=OverwriteStorage(), blank=True)
    is_active = models.BooleanField(default=True)
    hex_color = models.CharField(max_length=7, blank=True)

    active = ActiveManager()
    objects = active # objects = models.Manager()

    def __unicode__(self):
        return self.name

    #@method_cache(86400)
    def submit_statistics(self, first_date=None, last_date=None):
        params = [self.id]
        query = """
            SELECT vs.id, COUNT(wv.id) AS total, SUM(IF(passed = 1,1,0)) AS passed, SUM(IF(passed = 0,1,0)) AS failed, SUM(IF(passed = 1,1,0)) / COUNT(passed) * 100 AS percent, SUM(IF(proceeding_type = 'wetsvoorstel',1,0)) AS bills, SUM(IF(proceeding_type = 'motie',1,0)) AS motions, SUM(IF(proceeding_type = 'amendement',1,0)) AS amendments
            FROM (
                SELECT vote_id, wp.party_id AS id
                FROM website_vote_submitted_by AS ws
                JOIN website_politician AS wp ON ws.politician_id = wp.id
                WHERE wp.party_id = %s
            ) AS vs
            JOIN website_vote AS wv ON wv.id = vs.vote_id
            JOIN website_proceeding AS wr ON wr.id = wv.proceeding_id
            """
        if first_date and last_date:
            query += """
            WHERE wr.date_of_vote BETWEEN '%s' AND '%s'"""
            params.extend([first_date, last_date])

        for politician in Politician.objects.raw(query, params):  # todo exception?
            if politician.total > 0:
                return [int(politician.total), int(politician.passed), int(politician.failed),
                        round(politician.percent, 1), int(politician.motions), int(politician.amendments),
                        int(politician.bills)]
            return [0, 0, 0, 0.0, 0, 0, 0]

    #@method_cache(86400)
    def submit_support(self, first_date=None, last_date=None):
        params = [self.id]
        query = """
            SELECT *, abs_aye / total * 100 AS rel_aye, abs_no / total * 100 AS rel_no
            FROM (
                SELECT pa.id, pa.abbreviation, COUNT(vote_result) AS total, SUM(IF(vote_result = 1,1,0)) AS abs_aye, SUM(IF(vote_result = 0,1,0)) AS abs_no
                FROM (

                    SELECT vote_id, wp.party_id AS id
                    FROM website_vote_submitted_by AS ws
                    JOIN website_politician AS wp ON ws.politician_id = wp.id
                    WHERE wp.party_id = %s
                ) AS vs
                JOIN website_vote AS wv ON wv.id = vs.vote_id
                JOIN website_partyvote AS pv ON pv.vote_id = wv.id AND vs.id != pv.party_id
                JOIN website_party AS pa ON pv.party_id = pa.id
                """
        if first_date and last_date:
            query += """
                JOIN website_proceeding AS wr ON wr.id = wv.proceeding_id
                WHERE wr.date_of_vote BETWEEN '%s' AND '%s'"""
            params.extend([first_date, last_date])
        query += """
                GROUP BY pa.id
            ) AS a
            WHERE abs_aye != 0 OR abs_no != 0
            ORDER BY abs_aye DESC, total DESC, RAND()"""

        results = []
        for party in Party.objects.raw(query, params):  # todo exception?
            results.append(
                [party.abbreviation, int(party.total), int(party.abs_aye), int(party.abs_no), round(party.rel_aye, 1),
                 round(party.rel_no, 1)])
        return results

    def category_support(self, first_date=None, last_date=None, limit=3):
        params = [self.id, self.id]
        query = """
            SELECT prc.id, a.category, a.subcategory, SUM(IF(passed = 1,1,0)) AS passed, SUM(IF(passed = 0,1,0)) AS failed, SUM(IF(passed = 1,1,0)) / COUNT(passed) * 100 AS percent, total, SUM(IF(proceeding_type = 'wetsvoorstel',1,0)) AS bills, SUM(IF(proceeding_type = 'motie',1,0)) AS motions, SUM(IF(proceeding_type = 'amendement',1,0)) AS amendments
            FROM (
                SELECT vote_id, ws.politician_id AS id
                FROM website_vote_submitted_by AS ws
                JOIN website_politician AS po ON ws.politician_id = po.id
                WHERE po.party_id = %s
            ) AS vs
            JOIN website_vote AS wv ON wv.id = vs.vote_id
            JOIN website_proceeding AS pr ON pr.id = wv.proceeding_id
            JOIN website_proceeding_categories AS pc ON pr.id = pc.proceeding_id
            JOIN (
                SELECT pc.proceedingcategory_id, category, subcategory, COUNT(wv.id) AS total
                FROM (
                    SELECT vote_id
                    FROM website_vote_submitted_by AS ws
                    JOIN website_politician AS po ON ws.politician_id = po.id
                    WHERE po.party_id = %s
                ) AS vs
                JOIN website_vote AS wv ON wv.id = vs.vote_id
                JOIN website_proceeding AS wr ON wr.id = wv.proceeding_id
                JOIN website_proceeding_categories AS pc ON wr.id = pc.proceeding_id
                JOIN website_proceedingcategory AS prc ON prc.id = pc.proceedingcategory_id
                GROUP BY pc.proceedingcategory_id
                ORDER BY total DESC
                LIMIT 2
            ) AS a ON pc.proceedingcategory_id = a.proceedingcategory_id
            JOIN website_proceedingcategory AS prc ON a.proceedingcategory_id = prc.id
            """
        if first_date and last_date:
            query += """
            WHERE pr.date_of_vote BETWEEN '%s' AND '%s'"""
            params.extend([first_date, last_date])
        query += """
            GROUP BY prc.id
            ORDER BY total DESC"""

        results = []
        for category in ProceedingCategory.objects.raw(query, params):  # todo exception?
            #print round(party.rel_aye, 1), type(round(party.rel_aye, 1))
            results.append(
                {'category': category.category, 'subcategory': category.subcategory, 'passed': int(category.passed),
                 'failed': int(category.failed), 'percent': round(category.percent, 1), 'total': int(category.total),
                 'motions': int(category.motions), 'amendments': int(category.amendments), 'bills': int(category.bills)})
        return results

    class Meta:
        verbose_name = "Party"
        verbose_name_plural = "Parties"


class Coalition(models.Model):
    name = models.CharField(max_length=100, blank=True)
    type = models.NullBooleanField(blank=True, null=True, choices=((0, 'Oppositie'), (1, 'Regering')))
    party = models.ManyToManyField('Party')

    @property
    def title(self):
        return self.name or "%s: %s" % (self.get_type_display(), self.__unicode__())

    def __unicode__(self):
        return " - ".join([party.abbreviation for party in self.party.all()])


class Proceeding(PoliticalMashup):
    proceeding_type = models.CharField(max_length=100) #todo make num/bool field
    legislative_period = models.CharField(max_length=100)
    session_number = models.IntegerField()
    dossier_number = models.CharField(max_length=14)
    dossier_subnumber = models.CharField(max_length=8)
    date_of_vote = models.DateField()
    categories = models.ManyToManyField('ProceedingCategory', blank=True, null=True)
    title = models.CharField(max_length=600, blank=True) #todo too long
    house = models.IntegerField(choices=(('Eerste kamer', 0), ('Tweede kamer', 1)))

    #@property
    #def title(self):
    #    #if self._title:
    #    #    return self._title
    #    #else:
    #    return "%s ( %s, %s )" % (self.proceeding_type, self.dossier_number, self.dossier_subnumber)
    #
    #@title.setter
    #def set_title(self, input):
    #    self._title = input
    #
    def __unicode__(self):
        return self.pm_id

    class Meta:
        #unique_together = ['dossier_number', 'dossier_subnumber']
        pass


class Vote(models.Model):
    proceeding = models.OneToOneField('Proceeding')
    passed = models.BooleanField(default=True)
    submitted_by = models.ManyToManyField('Politician', blank=True, null=True)
    party_votes = models.ManyToManyField('Party', through='PartyVote', blank=True, null=True)

    def __unicode__(self):
        return self.proceeding.title


class PartyVote(models.Model):
    vote = models.ForeignKey('Vote')
    party = models.ForeignKey('Party')
    vote_result = models.NullBooleanField()
    explicit = models.NullBooleanField(blank=True, null=True)

    def __unicode__(self):
        try: #todo: temp
            return "%s - %s - %s" % (self.vote.proceeding.dossier_number, self.party.name, str(self.vote_result))
        except:
            return "%s - %s" % (self.party.name, str(self.vote_result))

    class Meta:
        unique_together = ['vote', 'party']


class ProceedingCategory(models.Model):
    category = models.CharField(max_length=40)
    subcategory = models.CharField(max_length=40)


class Commission(models.Model):
    name = models.CharField(max_length=100)
    subject = models.CharField(max_length=100)
    type = models.CharField(max_length=100)
    members = models.ManyToManyField('Politician')    #dossiers = models.ManyToManyField('Dossier', blank=True, null=True)